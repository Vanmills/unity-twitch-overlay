﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;

public class MoveSmallTicker : MonoBehaviour
{

    private bool selected = false;
    private Vector3 offset = Vector3.zero;
    
    // Use this for initialization
    void Start ()
    {
        Color color = GetComponent<SpriteRenderer>().material.color;
        color.a = 0.0f;
        GetComponent<SpriteRenderer>().material.color = color;
    }
	
	// Update is called once per frame
	void Update () {
	    if (selected)
	    {
	        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition) - offset;
	        Rect camRect = transform.parent.GetComponent<Camera>().rect;
            camRect.x = pos.x;
	        camRect.y = pos.y;
	        if (camRect.y <= 0.0f)
	        {
	            camRect.y = 0;
	        }
	        else if (camRect.y >= 1 - camRect.height)
	        {
	            camRect.y = 1 - camRect.height;
	        }
	        if (camRect.x >= 1 - camRect.width)
	        {
	            camRect.x = 1 - camRect.width;
	        } else if (camRect.x <= 0.0f)
	        {
	            camRect.x = 0;
	        }
	        transform.parent.GetComponent<Camera>().rect = camRect;

        }

	}

    void OnMouseDown()
    {
        selected = true;
        Debug.Log("Ticker selected");
        offset = new Vector3(0.05f, 0.025f);

        Color color = GetComponent<SpriteRenderer>().material.color;
        color.a = 0.25f;
        GetComponent<SpriteRenderer>().material.color = color;
    }

    void OnMouseUp()
    {
        selected = false;
        Color color = GetComponent<SpriteRenderer>().material.color;
        color.a = 0.0f;
        GetComponent<SpriteRenderer>().material.color = color;
    }

}
