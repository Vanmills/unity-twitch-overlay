﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public class RandomRotate : MonoBehaviour
    {
        public float rotationSpeed = 0.8f;
        private float randomTimeToRotate;
        private Quaternion originalRotation;
        private int direction = 1;

	    // Use this for initialization
	    void Start ()
	    {
	        randomTimeToRotate = Random.Range(3.5f, 5.0f);
	        originalRotation = transform.rotation;
            Debug.Log("Screen width: " + Screen.width + "   ScreenHeight: " + Screen.height);
	        direction = Random.Range(0, 2);
	        if (direction == 0)
	        {
	            direction = -1;
                transform.rotation = Quaternion.Euler(originalRotation.x, originalRotation.y, 359.99f);
            }
	    }
	
	    // Update is called once per frame
	    void Update ()
	    {
	        randomTimeToRotate -= Time.deltaTime;

	        if (randomTimeToRotate <= 0.0f)
	        {
	            StartRotation();
	        }

	    }

        private void StartRotation()
        {
            Vector3 eulerRot = transform.rotation.eulerAngles;
            Vector3 rotate = Vector3.zero;
            if (direction == 1)
            {
                rotate = Vector3.Lerp(eulerRot, new Vector3(eulerRot.x, eulerRot.y, 360.0f),
                    rotationSpeed*Time.deltaTime);
            } else if (direction == -1)
            {
                
                rotate = Vector3.Lerp(eulerRot, new Vector3(eulerRot.x, eulerRot.y, 0.0f),
                    rotationSpeed*Time.deltaTime);
            }

            transform.rotation = Quaternion.Euler(rotate);
            if (transform.rotation.eulerAngles.z >= 359.5f && direction == 1)
            {
                transform.rotation = originalRotation;
                randomTimeToRotate = Random.Range(3.5f, 5.0f);
                direction = Random.Range(0, 2);
                if (direction == 0)
                {
                    direction = -1;
                    transform.rotation = Quaternion.Euler(originalRotation.x, originalRotation.y, 359.99f);
                }

            } else if (direction == -1 && transform.rotation.eulerAngles.z <= 0.5f)
            {
                transform.rotation = originalRotation;
                randomTimeToRotate = Random.Range(3.5f, 5.0f);
                direction = Random.Range(0, 2);
                if (direction == 0)
                {
                    direction = -1;
                    transform.rotation = Quaternion.Euler(originalRotation.x, originalRotation.y, 359.99f);
                }
            }

        }

    }
}
