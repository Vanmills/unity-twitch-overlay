﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class TrayAnimationHandler : MonoBehaviour
{

    private bool startAnimation = true;
    private bool dirty = false;
    private bool autoAnimation = false;
    private float animationTimer = 0.0f;
    private float randomTimeFadeInFrom = 10.0f;
    private float randomTimeFadeInTo = 10;
    private float randomTimeFadeOutFrom = 10.0f;
    private float randomTimeFadeOutTo = 10;
    //private int alternator = 0;
    public bool fadeIn = false;

    void Awake()
    {
        
    }

	// Use 0this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (dirty)
	    {
	        dirty = false;
	        if (startAnimation)
	        {
                GetComponent<TrayAnimateOff>().StopAnimation();
	            GetComponent<TrayAnimateOn>().StartAnimation();
	        }
	        else
	        {
                GetComponent<TrayAnimateOn>().StopAnimation();
	            GetComponent<TrayAnimateOff>().StartAnimation();
            }
	    }

	    if (autoAnimation)
	    {
	        animationTimer -= Time.deltaTime;
	        if (animationTimer < 0.0f)
	        {
	            if (fadeIn)
	            {
	                fadeIn = false;
	                GetComponent<TrayAnimateOn>().StartAnimation();
	                GetComponent<TrayAnimateOff>().StopAnimation();
	                animationTimer = Random.Range(randomTimeFadeOutFrom, randomTimeFadeOutTo);
	            }
	            else
	            {
	                fadeIn = true;
                    GetComponent<TrayAnimateOn>().StopAnimation();
                    GetComponent<TrayAnimateOff>().StartAnimation();
	                animationTimer = Random.Range(randomTimeFadeInFrom, randomTimeFadeInTo);
	            }

	        }

	    }

	}

    public void TurnOnOffTray()
    {
        dirty = true;
        startAnimation = !startAnimation;
    }

    public void SetAutoAnimate()
    {
        autoAnimation = !autoAnimation;
    }

    public void SetAutomationTime(float pFadeInFrom, float pFadeInTo, float pFadeOutFrom, float pFadeOutTo)
    {
        // from int to float done deliberately
        randomTimeFadeInFrom = pFadeInFrom;
        randomTimeFadeInTo = pFadeInTo;
        randomTimeFadeOutFrom = pFadeOutFrom;
        randomTimeFadeOutTo = pFadeOutTo;

        //autoAnimation = true;
        animationTimer = 10.0f;
    }
}
