﻿using UnityEngine;
using System.Collections;

namespace TwitchOverlay
{
    public class EventHandler : MonoBehaviour
    {
        public delegate void NoArgDelegate();
        public delegate void SingleArgDelegate(object arg);
        public delegate void DualArgDelegate(object arg1, object arg2);

        public static event NoArgDelegate OnMoveIcon;
        public static event NoArgDelegate OnFadeOutInterface;
        public static event NoArgDelegate OnTurnOnOffTicker;

        public static event SingleArgDelegate OnReceivedPackage;
        public static event SingleArgDelegate OnReciveBGColor;
        public static event SingleArgDelegate OnReceivedImage;
        public static event SingleArgDelegate OnUpdateTickerText;
        public static event SingleArgDelegate OnUpdateTickerTextColor;
        public static event SingleArgDelegate OnUpdateTickerBGColor;
        public static event SingleArgDelegate OnAddImage;
        public static event SingleArgDelegate OnChangeWebChromaKey;
        public static event SingleArgDelegate OnAutomateTray;
        
        public static event SingleArgDelegate OnAddTray;
        public static event SingleArgDelegate OnTurnOnOffTray;

        public static event SingleArgDelegate OnAnimateImage;

        public static event SingleArgDelegate OnReceivedPreset;
        public static event SingleArgDelegate OnSetPresetTray;
        public static event SingleArgDelegate OnSetPresetGameImages;

        public static event DualArgDelegate OnReceiveSpeedAndDuration;

        private static void Handler(NoArgDelegate handler)
        {
            // ReSharper disable once UseNullPropagation - Doesn't work in .net 2.0 
            if (handler != null) handler();
        }

        private static void Handler(SingleArgDelegate handler, object arg)
        {
            //Debug.Log("Handled the event");
            if (handler != null && arg != null) handler(arg);
        }

        private static void Handler(DualArgDelegate handler, object arg1, object arg2)
        {
            if (handler != null && arg1 != null && arg2 != null) handler(arg1, arg2);
        }

        public static void OnMoveIcon_Click()
        {
            Handler(OnMoveIcon);
        }

        public static void TurnOnOffTicker()
        {
            Handler(OnTurnOnOffTicker);
        }

        public static void FadeOutInterfaceTrigger()
        {
            Handler(OnFadeOutInterface);
        }

        public static void IncomingPackage(object arg)
        {
            Handler(OnReceivedPackage, arg);
            Debug.Log("Package called");
        }

        public static void ChangeBgColor(object arg)
        {
            Handler(OnReciveBGColor, arg);
            Debug.Log("BG change called");
        }

        public static void ChangeImage(object arg)
        {
            Handler(OnReceivedImage, arg);
            Debug.Log("Image changed");
        }

        public static void UpdateTickerText(object arg)
        {
            Handler(OnUpdateTickerText, arg);
            Debug.Log("UpdatedText");
        }

        public static void UpdateTickerTextColor(object arg)
        {
            Handler(OnUpdateTickerTextColor, arg);
            Debug.Log("Updated ticker text color");
        }

        public static void UpdateTickerBackgroundColor(object arg)
        {
            Handler(OnUpdateTickerBGColor, arg);
            Debug.Log("Updated BG ticker color");
        }


        public static void AddImageToScene(object arg)
        {
            Handler(OnAddImage, arg);
            Debug.Log("Adding Image");
        }

        public static void ChangeWebCamChoromaKey(object arg)
        {
            Handler(OnChangeWebChromaKey, arg);
        }

        // Trays initiation 
        public static void AddTrayToScene(object arg)
        {
            Handler(OnAddTray, arg);
        }

        public static void TurnTrayOnOff(object arg)
        {
            Handler(OnTurnOnOffTray, arg);
        }

        public static void TurnOnAutoAnimation(object arg)
        {
            Handler(OnAutomateTray, arg);
        }

        public static void AddAnimatedImage(object arg)
        {
            Handler(OnAnimateImage, arg);
        }

        public static void ReceivedPreset(object arg)
        {
            Handler(OnReceivedPreset, arg);
        }

        public static void ReceivedTrayPreset(object arg)
        {
            Handler(OnSetPresetTray, arg);
        }

        public static void ReceivedGameImagePreset(object arg)
        {
            Handler(OnSetPresetGameImages, arg);
        }


        public static void ReceivedSpeedAndDuration(object arg1, object arg2)
        {
            Handler(OnReceiveSpeedAndDuration, arg1, arg2);
        }

    }
}
