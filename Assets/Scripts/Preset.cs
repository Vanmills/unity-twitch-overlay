﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

[Serializable]
public class Preset
{
    public string presetName = "";
    public bool resetPreset = false;
    public bool selectedPreset = false;
    public UserColor backgroundColor = new UserColor();
    public UserColor tickerBackgroundColor =  new UserColor();
    public UserColor tickerTextColor = new UserColor();
    public string tickerText;
    public bool useTickerBackground = true;
    public bool useTickerText = true;
    public float tickerSpeed = 1.5f;
    public float tickerRepeatTime = 35.0f;

    public List<Tray> listOfTrays = new List<Tray>();
    public List<MyGameObject> gameImages = new List<MyGameObject>();

    public static byte[] SerializeClass(Preset pPreset)
    {
        MemoryStream ms = new MemoryStream();
        BinaryWriter writer = new BinaryWriter(ms);

        writer.Write(pPreset.presetName);
        writer.Write(pPreset.resetPreset);
        writer.Write((byte)pPreset.backgroundColor.r * 255);
        writer.Write((byte)pPreset.backgroundColor.g * 255);
        writer.Write((byte)pPreset.backgroundColor.b * 255);

        writer.Write((byte)pPreset.tickerBackgroundColor.r * 255);
        writer.Write((byte)pPreset.tickerBackgroundColor.g * 255);
        writer.Write((byte)pPreset.tickerBackgroundColor.b * 255);

        writer.Write((byte)pPreset.tickerTextColor.r * 255);
        writer.Write((byte)pPreset.tickerTextColor.g * 255);
        writer.Write((byte)pPreset.tickerTextColor.b * 255);

        writer.Write(pPreset.tickerText);
        writer.Write(pPreset.useTickerBackground);
        writer.Write(pPreset.useTickerText);
        writer.Write(pPreset.tickerSpeed);
        writer.Write(pPreset.tickerRepeatTime);

        writer.Write(pPreset.listOfTrays.Count);
        for (int i = 0; i < pPreset.listOfTrays.Count; i++)
        {
            writer.Write(pPreset.listOfTrays[i].trayID);
            writer.Write(pPreset.listOfTrays[i].trayFilename);
            writer.Write(pPreset.listOfTrays[i].trayText);
            writer.Write((byte)pPreset.listOfTrays[i].trayTextColor.r * 255);
            writer.Write((byte)pPreset.listOfTrays[i].trayTextColor.g * 255);
            writer.Write((byte)pPreset.listOfTrays[i].trayTextColor.b * 255);
            writer.Write(pPreset.listOfTrays[i].trayFadeInSpeed);
            writer.Write(pPreset.listOfTrays[i].trayFadeOutSpeed);
            writer.Write((int)pPreset.listOfTrays[i].trayDirection);
            writer.Write(pPreset.listOfTrays[i].trayUseCountdown);
            writer.Write(pPreset.listOfTrays[i].trayCountdownMinutes);
            writer.Write(pPreset.listOfTrays[i].trayUseTimer);
            writer.Write(pPreset.listOfTrays[i].trayUseAutoAnimate);
            writer.Write(pPreset.listOfTrays[i].trayFadeInIntervalFromSeconds);
            writer.Write(pPreset.listOfTrays[i].trayFadeInIntervalToSeconds);
            writer.Write(pPreset.listOfTrays[i].trayFadeOutIntervalFromSeconds);
            writer.Write(pPreset.listOfTrays[i].trayFadeOutIntervalToSeconds);
        }
        writer.Write(pPreset.gameImages.Count);
        for (int i = 0; i < pPreset.gameImages.Count; i++)
        {
            writer.Write(pPreset.gameImages[i].ObjectFileName);
        }

        return ms.ToArray();
    }

    public static Preset DeserializeClass(byte[] buffer)
    {
        MemoryStream memStream = new MemoryStream(buffer);
        BinaryReader reader = new BinaryReader(memStream);
        Preset preset = new Preset();
        byte bgr, bgg, bgb;
        byte tbgr, tbgg, tbgb;
        byte ttr, ttg, ttb;

        preset.presetName = reader.ReadString();
        preset.resetPreset = reader.ReadBoolean();
        bgr = reader.ReadByte();
        bgg = reader.ReadByte();
        bgb = reader.ReadByte();
        preset.backgroundColor.HexColorToUnityColor(bgr, bgg, bgb);

        tbgr = reader.ReadByte();
        tbgg = reader.ReadByte();
        tbgb = reader.ReadByte();
        preset.tickerBackgroundColor.HexColorToUnityColor(tbgr, tbgg, tbgb);

        ttr = reader.ReadByte();
        ttg = reader.ReadByte();
        ttb = reader.ReadByte();
        preset.tickerTextColor.HexColorToUnityColor(ttr, ttg, ttb);
        
        preset.tickerText = reader.ReadString();
        preset.useTickerBackground = reader.ReadBoolean();
        preset.useTickerText = reader.ReadBoolean();
        preset.tickerSpeed = reader.ReadSingle();
        preset.tickerRepeatTime = reader.ReadSingle();

        int trayCount = reader.ReadInt32();

        for (int i = 0; i < trayCount; i++)
        {
            Tray tray = new Tray();
            byte r;
            byte g;
            byte b;

            tray.trayID = reader.ReadInt32();
            tray.trayFilename = reader.ReadString();
            tray.trayText = reader.ReadString();

            r = reader.ReadByte();
            g = reader.ReadByte();
            b = reader.ReadByte();
            tray.trayTextColor = new UserColor(r, g, b);
            tray.trayFadeInSpeed = reader.ReadInt32();
            tray.trayFadeOutSpeed = reader.ReadInt32();
            tray.trayDirection = (TrayDirection)reader.ReadInt32();
            tray.trayUseCountdown = reader.ReadBoolean();
            tray.trayCountdownMinutes = reader.ReadInt32();
            tray.trayUseTimer = reader.ReadBoolean();
            tray.trayUseAutoAnimate = reader.ReadBoolean();
            tray.trayFadeInIntervalFromSeconds = reader.ReadInt32();
            tray.trayFadeInIntervalToSeconds = reader.ReadInt32();
            tray.trayFadeOutIntervalFromSeconds = reader.ReadInt32();
            tray.trayFadeOutIntervalToSeconds = reader.ReadInt32();

            preset.listOfTrays.Add(tray);
        }
        int gameImagesCount = reader.ReadInt32();
        for (int i = 0; i < gameImagesCount; i++)
        {
            MyGameObject myGameObject = new MyGameObject();
            myGameObject.ObjectFileName = reader.ReadString();
            preset.gameImages.Add(myGameObject);
        }

        return preset;
    }

}
