﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;

public class NetManager : MonoBehaviour
{
    private NetServer server = new NetServer();

    // Use this for initialization
    void Awake () {
        server.Open("127.0.0.1", 63000);

    }

    void OnApplicationQuit()
    {
        if (server != null)
        {
            server.Close();
        }
    }

}
