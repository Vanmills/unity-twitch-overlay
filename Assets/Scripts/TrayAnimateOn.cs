﻿using UnityEngine;
using System.Collections;
using System.Runtime.Remoting.Channels;

public class TrayAnimateOn : MonoBehaviour
{

    //private Vector3 trayStartPosition;
    private Vector3 trayEndPosition;
    public bool startAnimation = false;

    public float speed = 1.5f;

    private TrayDirection animDirection;

    void Awake()
    {
        //trayEndPosition = new Vector3(13, transform.position.y, transform.position.z);
        //trayStartPosition = new Vector3(13.0f, transform.position.y, transform.position.z);
        animDirection = TrayDirection.Right;
    }

	// Use this for initialization
	void Start () {
	
        
	}
	
	// Update is called once per frame
	void Update () {
	    if (startAnimation)
	    {
            Vector3 pos = transform.position;
            if (animDirection == TrayDirection.Up)
            {
                float yPos = Mathf.Lerp(pos.y, trayEndPosition.y, speed*Time.deltaTime);
                if (yPos - trayEndPosition.y <= 0.01f)
                {
                    startAnimation = false;
                    return;
                }
                pos.y = yPos;
            }
            else if (animDirection == TrayDirection.Down)
            {
                float yPos = Mathf.Lerp(pos.y, trayEndPosition.y, speed * Time.deltaTime);
                if (trayEndPosition.y - yPos <= 0.01f)
                {
                    startAnimation = false;
                    return;
                }
                pos.y = yPos;
            }
            else if (animDirection == TrayDirection.Left)
            {
                float xPos = Mathf.Lerp(pos.x, trayEndPosition.x, speed*Time.deltaTime);
                if (trayEndPosition.x - xPos <= 0.01f)
                {
                    startAnimation = false;
                    return;
                }
                pos.x = xPos;
            }
            else if (animDirection == TrayDirection.Right)
            {
                float xPos = Mathf.Lerp(pos.x, trayEndPosition.x, speed*Time.deltaTime);
                if (xPos - trayEndPosition.x <= 0.01f)
                {
                    startAnimation = false;
                    return;
                }
                pos.x = xPos;
            }
            transform.position = pos;
	    }
	}

    public void SetEndPosition(Vector3 pEndpos)
    {
        trayEndPosition = pEndpos;
    }

    public void StartAnimation()
    {
        startAnimation = true;
        GetComponent<TrayAnimateOff>().StopAnimation();
    }

    public void SetAnimationDirection(TrayDirection pDirection)
    {
        animDirection = pDirection;
        //if (animDirection == TrayDirection.Right)
        //{
        //    trayStartPosition = new Vector3(23.0f, 0.0f, 0.0f);
        //} else if (animDirection == TrayDirection.Left)
        //{
        //    trayStartPosition = new Vector3(-23.0f, 0.0f, 0.0f);
        //} else if (animDirection == TrayDirection.Up)
        //{
        //    trayStartPosition = new Vector3(0.0f, 18.0f, 0.0f);
        //} else if (animDirection == TrayDirection.Down)
        //{
        //    trayStartPosition = new Vector3(0.0f, -18.0f, 0.0f);
        //}

    }

    public void StopAnimation()
    {
        startAnimation = false;
    }

    public void SetAnimationSpeed(int pSpeed)
    {
        speed = (float)pSpeed / 10.0f;
    }
}
