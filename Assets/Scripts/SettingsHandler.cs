﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using UnityEngine.Serialization;
using Debug = UnityEngine.Debug;

public class SettingsHandler : MonoBehaviour
{

    //public static List<GameObject> gameobjectsToSave = new List<GameObject>();
    private string settingFilename = ".\\settings-unity.xml";
    
    void Awake()
    {
        //TwitchOverlay.EventHandler.OnReciveBGColor += TwitchOverlay.EventHandler.SingleArgDelegate((color)=>)
    }
    
	// Use this for initialization
	void Start () {

        if (File.Exists(settingFilename))
        {
            LoadSettings();
        }
	    else
	    {
            Preset resetSceneSettings = new Preset();
            resetSceneSettings.presetName = "Reset preset";
            resetSceneSettings.backgroundColor = new UserColor(Camera.main.backgroundColor);
            resetSceneSettings.tickerBackgroundColor = new UserColor(59.0f / 255.0f, 101.0f / 255.0f, 164.0f / 255.0f, 1.0f);
            resetSceneSettings.tickerTextColor = new UserColor(1.0f, 1.0f, 1.0f, 1.0f);
            resetSceneSettings.tickerText = "Ticker text not set - use the control panel to set the text";
            resetSceneSettings.selectedPreset = false;
            resetSceneSettings.tickerSpeed = 50.0f;
            resetSceneSettings.tickerRepeatTime = 35.0f;
            SaveObject.Instance.listOfPresets.Add(resetSceneSettings);

            Preset defaultSettings = new Preset();
	        defaultSettings.presetName = "Default preset";
            defaultSettings.backgroundColor = new UserColor(Camera.main.backgroundColor);
            defaultSettings.tickerBackgroundColor = new UserColor(59.0f/255.0f, 101.0f/255.0f, 164.0f/255.0f, 1.0f);
            defaultSettings.tickerTextColor = new UserColor(1.0f, 1.0f, 1.0f, 1.0f);
	        defaultSettings.tickerText = "Ticker text not set - use the control panel to set the text";
	        defaultSettings.selectedPreset = true;
	        defaultSettings.tickerSpeed = 50.0f;
	        defaultSettings.tickerRepeatTime = 35.0f;
            SaveObject.Instance.listOfPresets.Add(defaultSettings);
	        LoadData();

	    }
	}

    void OnApplicationQuit()
    {
        Save();
    }


    public void Save()
    {
        if (SaveObject.Instance.currentPreset.presetName == "Default preset")
        {
            SaveObject.Instance.listOfPresets[1] = SaveObject.Instance.currentPreset;
        }
        else
        {
            bool found = false;
            for (int i = 0; i < SaveObject.Instance.listOfPresets.Count; i++)
            {
                if (SaveObject.Instance.currentPreset.presetName == SaveObject.Instance.listOfPresets[i].presetName)
                {
                    SaveObject.Instance.listOfPresets[i] = SaveObject.Instance.currentPreset;
                    found = true;
                }
            }
            if (!found)
            {
                SaveObject.Instance.listOfPresets.Add(SaveObject.Instance.currentPreset);
            }
        }


        try
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<Preset>));
            Stream stream = new FileStream(settingFilename, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, SaveObject.Instance.listOfPresets);
            stream.Flush();
            stream.Close();

        }
        catch (Exception exception)
        {
            Debug.Log("Error: " + exception.Message);
        }
    }

    public void LoadSettings()
    {
        XmlSerializer formatter = new XmlSerializer(typeof(List<Preset>));
        Stream stream = new FileStream(settingFilename, FileMode.Open, FileAccess.Read,
                        FileShare.ReadWrite);
        SaveObject.Instance.listOfPresets = (List<Preset>) formatter.Deserialize(stream);
        stream.Flush();
        stream.Close();
        

        LoadData();
    }

    private void LoadData()
    {
        bool found = false;
        for (int i = 0; i < SaveObject.Instance.listOfPresets.Count; i++)
        {
            if (SaveObject.Instance.listOfPresets[i].selectedPreset)
            {
                SaveObject.Instance.currentPreset = SaveObject.Instance.listOfPresets[i];
                found = true;
            }
        }
        //Preset defaultPreset = SaveObject.Instance.currentPreset;
        if (!found)
        {
            
            SaveObject.Instance.currentPreset = SaveObject.Instance.listOfPresets[1];
        }
        TwitchOverlay.EventHandler.ReceivedPreset(SaveObject.Instance.currentPreset);


    }

}




//if (xmlElementList[i].Name == "GameImagePosition")
//{
//    for (int j = 0; j < subElements.Count; j++)
//    {
//        if (subElements[j].Name == "x")
//        {
//            saveObject.GameImagePosition.x = float.Parse(subElements[j].Value);
//        }
//        else if (subElements[j].Name == "y")
//        {
//            saveObject.GameImagePosition.y = float.Parse(subElements[j].Value);
//        }
//        else if (subElements[j].Name == "z")
//        {
//            saveObject.GameImagePosition.z = float.Parse(subElements[j].Value);
//        }
//    }
//}
//if (xmlElementList[i].Name == "GameImageScale")
//{
//    for (int j = 0; j < subElements.Count; j++)
//    {
//        if (subElements[j].Name == "x")
//        {
//            saveObject.GameImageScale.x = float.Parse(subElements[j].Value);
//        }
//        else if (subElements[j].Name == "y")
//        {
//            saveObject.GameImageScale.y = float.Parse(subElements[j].Value);
//        }
//        else if (subElements[j].Name == "z")
//        {
//            saveObject.GameImageScale.z = float.Parse(subElements[j].Value);
//        }
//    }
//}

//SaveObject.Instance.TickerBackgroundColor = saveObject.TickerBackgroundColor;
//formatter = new XmlSerializer(typeof(List<MyGameObject>));
//stream = new FileStream(".\\unity-savedImages.xml", FileMode.Open, FileAccess.Read,
//                FileShare.ReadWrite);
//List<MyGameObject> imageList = (List<MyGameObject>)formatter.Deserialize(stream);
//Debug.Log("Read " + imageList.Count + " images!");
//for (int i = 0; i < imageList.Count; i++)
//{
//    SaveObject.Instance.currentPreset.gameImages.Add(imageList[i]);
//}

//stream.Flush();
//stream.Close();



//public void Load()
//{
//    SaveObject saveObject = SaveObject.Instance;
//    try
//    {
//        string xmlContent = File.ReadAllText(".\\settings.xml");
//        XElement xmlElements = XElement.Parse(xmlContent);

//        //Debug.Log("Filecontent: " + @xmlContent);
//        List<XElement> xmlElementList = xmlElements.Elements().ToList();
//        for (int i = 0; i < xmlElementList.Count; i++)
//        {
//            List<XElement> subElements = xmlElementList[i].Elements().ToList();
//            if (xmlElementList[i].Name == "WindowBGColor")
//            {
//                for (int j = 0; j < subElements.Count; j++)
//                {

//                    if (subElements[j].Name == "r")
//                    {
//                        saveObject.WindowBGColor.r = float.Parse(subElements[j].Value);
//                    }
//                    else if (subElements[j].Name == "g")
//                    {
//                        saveObject.WindowBGColor.g = float.Parse(subElements[j].Value);
//                    }
//                    else if (subElements[j].Name == "b")
//                    {
//                        saveObject.WindowBGColor.b = float.Parse(subElements[j].Value);
//                    }
//                    else if (subElements[j].Name == "a")
//                    {
//                        saveObject.WindowBGColor.a = float.Parse(subElements[j].Value);
//                    }
//                }
//            }
//            if (xmlElementList[i].Name == "TickerTextColor")
//            {
//                for (int j = 0; j < subElements.Count; j++)
//                {
//                    if (subElements[j].Name == "r")
//                    {
//                        saveObject.TickerTextColor.r = float.Parse(subElements[j].Value);
//                    }
//                    else if (subElements[j].Name == "g")
//                    {
//                        saveObject.TickerTextColor.g = float.Parse(subElements[j].Value);
//                    }
//                    else if (subElements[j].Name == "b")
//                    {
//                        saveObject.TickerTextColor.b = float.Parse(subElements[j].Value);
//                    }
//                    else if (subElements[j].Name == "a")
//                    {
//                        saveObject.TickerTextColor.a = float.Parse(subElements[j].Value);
//                    }
//                }
//            }

//            if (xmlElementList[i].Name == "TickerBackgroundColor")
//            {
//                for (int j = 0; j < subElements.Count; j++)
//                {
//                    if (subElements[j].Name == "r")
//                    {
//                        saveObject.TickerBackgroundColor.r = float.Parse(subElements[j].Value);
//                    }
//                    else if (subElements[j].Name == "g")
//                    {
//                        saveObject.TickerBackgroundColor.g = float.Parse(subElements[j].Value);
//                    }
//                    else if (subElements[j].Name == "b")
//                    {
//                        saveObject.TickerBackgroundColor.b = float.Parse(subElements[j].Value);
//                    }
//                    else if (subElements[j].Name == "a")
//                    {
//                        saveObject.TickerBackgroundColor.a = float.Parse(subElements[j].Value);
//                    }
//                }
//            }


//            if (xmlElementList[i].Name == "TickerMessage")
//            {
//                saveObject.TickerMessage = xmlElementList[i].Value;
//            }
//            if (xmlElementList[i].Name == "GameImage")
//            {
//                saveObject.GameImage = xmlElementList[i].Value;
//            }

//            if (xmlElementList[i].Name == "TickerOnOff")
//            {
//                if (xmlElementList[i].Value == "true")
//                {
//                    SaveObject.Instance.TickerOn = true;
//                }
//                else
//                {
//                    SaveObject.Instance.TickerOn = false;
//                }
//            }

//        }
//        Color color = new Color(saveObject.WindowBGColor.r, saveObject.WindowBGColor.g, saveObject.WindowBGColor.b);
//        TwitchOverlay.EventHandler.ChangeBgColor(color);
//        if (!string.IsNullOrEmpty(saveObject.GameImage))
//        {
//            TwitchOverlay.EventHandler.ChangeImage(saveObject.GameImage);
//        }
//        if (!string.IsNullOrEmpty(saveObject.TickerMessage))
//        {
//            TwitchOverlay.EventHandler.UpdateTickerText(saveObject.TickerMessage);
//        }
//        else
//        {
//            TwitchOverlay.EventHandler.UpdateTickerText("No text set. Use the control panel to set a more fitting text!");
//        }
//        if (saveObject.TickerOn == true)
//        {
//            TwitchOverlay.EventHandler.TurnOnOffTicker();
//        }
//        if (saveObject.TickerBackgroundColor != null)
//        {
//            Color tickerBGColor = new Color(saveObject.TickerBackgroundColor.r, saveObject.TickerBackgroundColor.g, saveObject.TickerBackgroundColor.b);
//            TwitchOverlay.EventHandler.UpdateTickerBackgroundColor(tickerBGColor);
//        }
//        if (saveObject.TickerTextColor != null)
//        {
//            Color tickerTextColor = new Color(saveObject.TickerTextColor.r, saveObject.TickerTextColor.g, saveObject.TickerTextColor.b);
//            TwitchOverlay.EventHandler.UpdateTickerTextColor(tickerTextColor);
//        }

//    }
//    catch (Exception exception)
//    {
//        Debug.Log("Error: " + exception.Message);
//        return;
//    }
//}


//Color color = defaultPreset.backgroundColor.GetUnityColor();
//TwitchOverlay.EventHandler.ChangeBgColor(color);
////if (!string.IsNullOrEmpty(saveObject.GameImage))
////{
////    TwitchOverlay.EventHandler.ChangeImage(saveObject.GameImage);
////}
//if (!string.IsNullOrEmpty(SaveObject.Instance.currentPreset.tickerText))
//{
//    TwitchOverlay.EventHandler.UpdateTickerText(SaveObject.Instance.currentPreset.tickerText);
//}
//else
//{
//    TwitchOverlay.EventHandler.UpdateTickerText("No text set. Use the control panel to set a more fitting text!");
//}
//if (SaveObject.Instance.currentPreset.useTickerText == true)
//{
//    TwitchOverlay.EventHandler.TurnOnOffTicker();
//}
//if (SaveObject.Instance.currentPreset.useTickerBackground == false)
//{
//    TwitchOverlay.EventHandler.FadeOutInterfaceTrigger();
//}
//if (SaveObject.Instance.currentPreset.tickerBackgroundColor != null)
//{
//    Color tickerBGColor = SaveObject.Instance.currentPreset.tickerBackgroundColor.GetUnityColor();
//    TwitchOverlay.EventHandler.UpdateTickerBackgroundColor(tickerBGColor);
//}
//if (SaveObject.Instance.currentPreset.tickerTextColor != null)
//{
//    Color tickerTextColor = SaveObject.Instance.currentPreset.tickerTextColor.GetUnityColor();
//    TwitchOverlay.EventHandler.UpdateTickerTextColor(tickerTextColor);
//}

//TwitchOverlay.EventHandler.ReceivedSpeedAndDuration(SaveObject.Instance.currentPreset.tickerSpeed, SaveObject.Instance.currentPreset.tickerRepeatTime);
