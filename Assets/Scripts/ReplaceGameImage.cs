﻿using System;
using UnityEngine;
using System.Collections;
using System.IO;

[Serializable]
public class ReplaceGameImage : MonoBehaviour
{

    private bool dirty = false;
    private Texture2D texture;
    private Texture2D oldTexture2D;
    private string filename = "";

    void Awake()
    {
        TwitchOverlay.EventHandler.OnReceivedImage += new TwitchOverlay.EventHandler.SingleArgDelegate((filename)=>ChangeImage((string)filename));
    }

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	    if (dirty)
	    {

	        dirty = false;
            SpriteRenderer rend = gameObject.GetComponent<SpriteRenderer>();
	        oldTexture2D = rend.sprite.texture;
	        try
	        {
	            byte[] buffer = File.ReadAllBytes(filename);
                texture = new Texture2D(512, 512, TextureFormat.DXT5, false);
                texture.LoadImage(buffer);
                Debug.Log("Texture size: " + texture.width + " " + texture.height);
	        
                //www.LoadImageIntoTexture(texture);
                //gameObject.GetComponent<
                rend.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
	            rend.sprite.name = Path.GetFileNameWithoutExtension(filename);
                gameObject.GetComponent<BoxCollider2D>().size = new Vector2(texture.width / 100, texture.height/100);

	        }
	        catch (IOException exception)
	        {
                rend.sprite = Sprite.Create(oldTexture2D, new Rect(0, 0, oldTexture2D.width, oldTexture2D.height),new Vector2(0.5f, 0.5f));
                Debug.Log(exception.Message);
	        }
	        
	        //Texture tex = new Texture();
	        
	    }
	}

    public void ChangeImage(string pfilename)
    {
        filename = pfilename;
        //Sprite spr = new Sprite();
        dirty = true;
        //SaveObject.Instance.GameImage = filename;
    }

}
