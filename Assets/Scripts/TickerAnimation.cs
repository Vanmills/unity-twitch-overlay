﻿using System;
using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    [Serializable]
    public class TickerAnimation : MonoBehaviour
    {

        private float tickerSpeed = 0.1f;
        public float endPosition = 35.0f;
        //private float 
        //private float speedincrease = 0.0f;

        public Vector3 originalPosition;
        public bool animationStart = true;
        private string tickertext = "";
        private bool dirty = false;
        private bool onOffDirty = false;
        private Color color;
        //private float randomRerun = 0.0f;

        void Awake()
        {
            TwitchOverlay.EventHandler.OnUpdateTickerTextColor += new TwitchOverlay.EventHandler.SingleArgDelegate((newColor)=>ChangeColor((Color)newColor));
            //TwitchOverlay.EventHandler.OnTurnOnOffTicker += new TwitchOverlay.EventHandler.NoArgDelegate(SetOnOff);
        }

	    // Use this for initialization
	    void Start ()
	    {
	        originalPosition = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
            //TwitchOverlay.EventHandler.OnUpdateText += new TwitchOverlay.EventHandler.SingleArgDelegate((text)=>SetText((string)text));
	    }
	
	    // Update is called once per frame
	    void Update ()
	    {
	        if (animationStart)
	        {
	            //speedincrease += Time.deltaTime;
	            transform.position = Vector3.Lerp(transform.position, new Vector3(endPosition, transform.position.y, transform.position.z), tickerSpeed * Time.deltaTime);
	            if (endPosition < 0)
	            {
                    if (transform.position.x - endPosition < 0.1f)
	                {
                        animationStart = false;
	                    //speedincrease = 0.0f;
	                }
  	            } else if (endPosition > 0)
  	            {
                    if (endPosition - transform.position.x < 0.1f)
                    {
                        animationStart = false;
                        //speedincrease = 0.0f;
                    }
                }
	        }
            if (dirty)
            {
                dirty = false;
                GetComponent<TextMesh>().color = color;
            }
	        if (onOffDirty)
	        {
	            onOffDirty = false;
	            GetComponent<SpriteRenderer>().enabled = !GetComponent<SpriteRenderer>().enabled;
	        }

	    }

        public void RestartAnimation()
        {
            animationStart = true;
            transform.position = originalPosition;
        }

        public void SetText(string text)
        {
            tickertext = text;
            transform.position = originalPosition;
            GetComponent<TextMesh>().text = tickertext;
            animationStart = true;
        }

        private void ChangeColor(Color pColor)
        {
            color = pColor;
            dirty = true;
            SaveObject.Instance.currentPreset.tickerTextColor = new UserColor(color.r, color.g, color.b, color.a);
        }

        public void SetSpeed(float pSpeed)
        {
            tickerSpeed = pSpeed;
        }

    }
}
