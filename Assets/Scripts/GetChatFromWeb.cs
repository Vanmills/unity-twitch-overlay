﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GetChatFromWeb : MonoBehaviour
{
    //https://www.twitch.tv/sanctuary_of_wolves/chat?popout=

    //public string url = "https://www.twitch.tv/sanctuary_of_wolves/chat?popout=";
    public string url = "http://www.vg.no";

    private void Start()
    {
        FetchWebpage();
    }

    private IEnumerator FetchWebpage()
    {
        WWW www = new WWW(url);
        yield return www;
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = www.texture;
        //renderer.material.color = Color.white;
        //Image img = GetComponent<Image>(); 
        //img = (Image)www.texture;
    }
}