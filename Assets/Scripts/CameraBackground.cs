﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[Serializable]
public class CameraBackground : MonoBehaviour
{
    private Color bgColor;
    private bool dirty = false;

    void Awake()
    {
        TwitchOverlay.EventHandler.OnReciveBGColor += new TwitchOverlay.EventHandler.SingleArgDelegate((color) => BackgroundChange((Color)color));

    }

	// Use this for initialization
	void Start () {

    }

    // Update is called once per frame
    void Update () {
	    if (dirty)
	    {
	        dirty = false;
	        Camera.main.backgroundColor = bgColor;
	    }
	}

    public void BackgroundChange(Color color)
    {
        bgColor = color;
        dirty = true;
        Debug.Log("Got color information");
        SaveObject.Instance.currentPreset.backgroundColor = new UserColor(color.r, color.g, color.b, color.a);
    }

}
