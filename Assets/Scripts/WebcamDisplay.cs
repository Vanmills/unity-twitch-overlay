﻿using System;
using UnityEngine;

public class WebcamDisplay : MonoBehaviour
{
    private WebCamTexture webCamTexture;
    private Color32 chromaKey = new Color32(0, 0, 0, 0);
    private Texture2D texture;
    //private ColorHSL chromaColorHSL = new ColorHSL();
    //private int frameskipper = 0;
    private Color32[] webCamColors;

    void Awake()
    {
        TwitchOverlay.EventHandler.OnChangeWebChromaKey += new TwitchOverlay.EventHandler.SingleArgDelegate((chromaKeyChange)=>ChangeChromaKey((Color32)chromaKeyChange));
    }

    // Use this for initialization
    void Start ()
	{
        webCamTexture = new WebCamTexture(640, 480);
        //webCamTexture.Play();
        texture = new Texture2D(webCamTexture.requestedWidth, webCamTexture.requestedHeight);
    }

    void OnApplicationQuit()
    {
        webCamTexture.Stop();
    }

	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.P))
	    {
	        if (webCamTexture.isPlaying)
	        {
	            webCamTexture.Stop();
	            GetComponent<SpriteRenderer>().enabled = false;
	        }
	        else
	        {
                GetComponent<SpriteRenderer>().enabled = true;
                webCamTexture.Play();
	        }
	    }
	    if (webCamTexture.isPlaying && webCamTexture.didUpdateThisFrame)
	    {
	        Color32[] oldWebcamColor32s = webCamColors;
            webCamColors = webCamTexture.GetPixels32();
            // Color32 whiteColor = new Color32(255, 255, 255, 255);

            for (int i = 0; i < webCamColors.Length; i++)
            {
                if (oldWebcamColor32s != null)
                {
                    if (oldWebcamColor32s[i].a == 0 &&
                        webCamColors[i].r == oldWebcamColor32s[i].r &&
                        webCamColors[i].g == oldWebcamColor32s[i].g &&
                        webCamColors[i].b == oldWebcamColor32s[i].b)
                    {
                        webCamColors[i].a = 0;
                    }
                    else
                    {
                        if (chromaKey.g >= 250)
                        {
                            if (webCamColors[i].g > 170 && webCamColors[i].r < 160 && webCamColors[i].b < 140)
                            {
                                webCamColors[i].a = 0;
                            }
                        } else if (chromaKey.r >= 250)
                        {
                            if (webCamColors[i].r > 170 && webCamColors[i].g < 160 && webCamColors[i].b < 140)
                            {
                                webCamColors[i].a = 0;
                            }
                        } else if (chromaKey.b >= 250)
                        {
                            if (webCamColors[i].b > 170 && webCamColors[i].r < 160 && webCamColors[i].g < 140)
                            {
                                webCamColors[i].a = 0;
                            }
                        }

                    }
                }
            }
            texture.SetPixels32(webCamColors);
            texture.Apply();
            GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture, new Rect(0, 0, webCamTexture.requestedWidth, webCamTexture.requestedHeight), new Vector2(0.5f, 0.5f));
            gameObject.GetComponent<BoxCollider2D>().size = new Vector2(texture.width / 100, texture.height / 100);
        }

	}


    private void ChangeChromaKey(Color32 pColor)
    {
        chromaKey = pColor;
        Debug.Log(chromaKey);
    }



}









// graveyard
//if (chromaKey.r > webCamColors[i].r)
//               {
//                   rSimilarities = chromaKey.r - webCamColors[i].r;
//               }
//               else
//               {
//                   rSimilarities = webCamColors[i].r - chromaKey.r;
//               }
//               if (chromaKey.g > webCamColors[i].g)
//               {
//                   gSimilarities = chromaKey.g - webCamColors[i].g;
//               }
//               else
//               {
//                   gSimilarities = webCamColors[i].g - chromaKey.g;
//               }
//               if (chromaKey.b > webCamColors[i].b)
//               {
//                   bSimilarities = chromaKey.b - webCamColors[i].b;
//               }
//               else
//               {
//                   bSimilarities = webCamColors[i].b - chromaKey.b;
//               }
//               r = webCamColors[i].r;
//               g = webCamColors[i].g;
//               b = webCamColors[i].b;

//               if (r > g && r > b)
//               {
//                   // red major
//                   if (g > b)
//                   {
//                       // with green submajor
//                       if (rSimilarities <= highDiff && gSimilarities <= midDiff && bSimilarities <= lowDiff)
//                       {
//                           webCamColors[i].a = 0;
//                       }

//                   } else if (g == b)
//                   {
//                       if (rSimilarities <= highDiff && gSimilarities <= midDiff && bSimilarities <= midDiff)
//                       {
//                           webCamColors[i].a = 0;
//                       }
//                   }
//                   else
//                   {
//                       if (rSimilarities <= highDiff && gSimilarities <= lowDiff && bSimilarities <= midDiff)
//                       {
//                           webCamColors[i].a = 0;
//                       }
//                   }
//               } else if (g > r && g > b)
//               {
//                   // green major
//                   if (r > b)
//                   {
//                       // with green submajor
//                       if (gSimilarities <= highDiff && rSimilarities <= midDiff && bSimilarities <= lowDiff)
//                       {
//                           webCamColors[i].a = 0;
//                       }

//                   }
//                   else if (r == b)
//                   {
//                       if (gSimilarities <= highDiff && bSimilarities <= midDiff && rSimilarities <= midDiff)
//                       {
//                           webCamColors[i].a = 0;
//                       }
//                   }
//                   else
//                   {
//                       if (gSimilarities <= highDiff && rSimilarities <= lowDiff && bSimilarities <= midDiff)
//                       {
//                           webCamColors[i].a = 0;
//                       }
//                   }

//               } else if (b > r && b > g)
//               {
//                   // blue major
//                   if (r > b)
//                   {
//                       // with green submajor
//                       if (bSimilarities <= highDiff && rSimilarities <= midDiff && gSimilarities <= lowDiff)
//                       {
//                           webCamColors[i].a = 0;
//                       }

//                   }
//                   else if (r == b)
//                   {
//                       if (bSimilarities <= highDiff && rSimilarities <= midDiff && gSimilarities <= midDiff)
//                       {
//                           webCamColors[i].a = 0;
//                       }
//                   }
//                   else
//                   {
//                       if (bSimilarities <= highDiff && rSimilarities <= lowDiff && gSimilarities <= midDiff)
//                       {
//                           webCamColors[i].a = 0;
//                       }
//                   }
//               }






//private bool TestColor(Color32 pCol)
//{

//    ColorHSL color = new ColorHSL();
//    color.ConvertToHSL(pCol);

//    int max = (int)Mathf.Max(color.H, chromaColorHSL.H);
//    int min = (int) Mathf.Min(color.H, chromaColorHSL.H);
//    if (max - min <= 2)
//    {
//        return true;
//    }

//    return false;
//}



//TestColor(chromaKey);
//chromaColorHSL.ConvertToHSL(chromaKey);
//Debug.Log("H:" + chromaColorHSL.H + " S:" + chromaColorHSL.S + " L:" + chromaColorHSL.L);

