﻿using System;
using UnityEngine;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Emit;
using System.Text;


class PipePackage
{
    public enum Packagetype
    {
        NotAValidPackage = 0,
        Background = 1,
        Layout = 2,
        Animation = 4,
        Information = 5,
        FadeOutTicker = 6,
        TickerColor = 7,
        TickerTextOnOff = 8,
        TickerBackgroundColor = 9,
        ImageFile = 10,
        ImageLocation = 11,
        WebcamChromaKey = 12,
        AddTray = 13,
        TrayAddedOk = 14,
        TrayOnOff = 15,
        TrayAutoAnimate = 16,
        Preset = 17,
        Quit = 255,
    }

    public Packagetype type = 0;
    public int bufferSize;
    public byte[] buffer;
    public string clearText;

    public byte[] CreateBuffer(string pBuff, Packagetype pType)
    {
        buffer = new byte[pBuff.Length + sizeof(Packagetype) + sizeof(int)];
        byte[] header = new byte[sizeof(Packagetype)];
        header = BitConverter.GetBytes((int)pType).Take(sizeof(Packagetype)).ToArray();
        byte[] bufferLength = new byte[sizeof(int)];
        bufferLength = BitConverter.GetBytes(pBuff.Length).Take(sizeof(int)).ToArray();
        int i = 0;
        Array.Reverse(header);
        Array.Reverse(bufferLength);

        for (; i < header.Length; i++)
        {
            buffer[i] = header[i];
        }
        for (; i < bufferLength.Length; i++)
        {
            buffer[i] = bufferLength[i];
        }

        byte[] stringBuffer = Encoding.ASCII.GetBytes(pBuff);

        for (; i < pBuff.Length; i++)
        {
            buffer[i] = stringBuffer[i - header.Length - bufferLength.Length];
        }
        return buffer;
    }

    public void DecodeBuffer(byte[] pBytesbuffer)
    {
        Debug.Log("Decoding..... Stay tuned");
        int i = 0;
        //int enumBufferTest = 0;
        byte[] enumArray = new byte[sizeof (Packagetype)];
        for (; i < sizeof(Packagetype); i++) // this will work for now until we have 255 options, think we can live with that.
        {
            enumArray[i] = pBytesbuffer[i];
            //enumBufferTest += pBytesbuffer[i];
            //Debug.Log("i: " + i + "    - " + (int)pBytesbuffer[i]);
        }
        Array.Reverse(enumArray);
        type = (Packagetype) BitConverter.ToInt32(enumArray, 0);
        Debug.Log("Type of enum " + type.ToString());

        int bufferLength = 0;
        byte[] bufferLengthArray = new byte[sizeof(int)];
        for (int j = 0; j < sizeof (int); i++, j++)
        {
            bufferLengthArray[j] = pBytesbuffer[i];
            
        }
        Array.Reverse(bufferLengthArray);
        bufferLength = BitConverter.ToInt32(bufferLengthArray, 0);
        bufferSize = bufferLength;

        buffer = new byte[bufferLength];
        for (int j = 0; j < bufferLength; i++)
        {
            buffer[j] = pBytesbuffer[i];
            j++;
        }
        clearText = Encoding.ASCII.GetString(buffer);
    }
}



/*Code graveyard*/


// Debug typemessages... Works removing
//switch (type)
//{
//    case Packagetype.Animation:
//    {
//        Debug.Log("Animation call");
//        break;
//    }
//    case Packagetype.Background:
//    {
//        Debug.Log("Background call");
//        break;
//    }
//    case Packagetype.Layout:
//    {
//        Debug.Log("Layout change call");
//        break;
//    }
//    case Packagetype.Information:
//    {
//        Debug.Log("Information call");
//        break;
//    }

//    default:
//    {
//        Debug.Log("Unknown call");
//        break;
//    }
//}


//byte[] typeBuffer = new byte[sizeof(Packagetype)];
//Debug.Log("cleartext received: " + clearText);
//Debug.Log("buffer length: " + bufferSize + " buffer array length: " + buffer.Length);
//bufferSize = buffer.Length;
//type = (Packagetype)Enum.Parse(typeof (Packagetype), typeBuffer.ToString());
//Debug.Log("found type, the type is: " + type + " - I is at this point: " + i);
//Debug.Log("Incoming buffer is " + pBytesbuffer.Length);
//Debug.Log("i: " + i + "    - "  + (int)pBytesbuffer[i]);
//type = (Packagetype) enumBufferTest;
//typeBuffer[i] = pBytesbuffer[i];
//for (int b = 0; b < pBytesbuffer.Length; b++)
//{
//    Debug.Log("b: " + b + "   ->" + pBytesbuffer[b]);
//}
