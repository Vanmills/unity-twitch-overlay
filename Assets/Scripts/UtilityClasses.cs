﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

/*
Small utility classes to "convert" the colors, vectors etc.
Only used here, so I kept them in the same file

*/



[Serializable]
public class ImageAnimation
{
    public int ID = -1;
    public string filenameGraphic = "";
    public float totalDuration = 10.0f; // total accessible timeline


    // Fade In
    public bool fadeIn = false;
    public float fadeInStartTime = 0.0f; // when should the fadein start in the timeline
    public float fadeInDuration = 0.5f; // how long is the fading animation going to last?

    // Fade Out
    public bool fadeOut = true;
    public float fadeOutStartTime = 9.0f; // When in the timeline should the fadeout start?
    public float fadeOutDuration = 1.0f; // how long should the fadeout last?

    // Rotate In
    public bool rotateIn = true; // Do you want the animation to rotate in?
    public bool rotateInClockwise = true; // Do you want the rotation to be clockwise or counter clockwise?
    public float rotateInStartTime = 0.1f; // When do you want the rotation to start in the timeline?
    public float rotateInDuration = 2.0f; // How long do you want the rotation to be?
    public float rotationInDegrees = 720.0f; // degrees to rotate during the duration of the in rotation;

    // Rotate Out
    public bool rotateOut = true; // Do you want the animation to rotate out?

    public bool rotateOutClockwise = true;
        // Do you want the out rotation animation to be clockwise or counter clockwise?

    public float rotateOutStartTime = 7.0f; // When in the timeline should the out rotation start?
    public float rotateOutDuration = 2.0f; // How long should the out rotation last?
    public float rotationOutDegrees = 720.0f; // How many degrees should the rotation be?

    // Zoom In
    public bool zoomIn = true; // Do you want to zoom the animation in?
    public float zoomInStartTime = 0.0f; // When do you want the start to occur in the timeline?
    public float zoomInSpeed = 2.0f; // How do you want the zoom In to last?
    public float zoomInSize = 1.35f;

    // Zoom out
    public bool zoomOut = true; // Do you want the animation to zoom out?
    public float zoomOutStartTime = 0.0f;
    public float zoomOutSpeed = 2.0f;
    public float zoomOutSize = 0.0f;

    // Zoom alternatives to be implemented later
    // Height / width zoom levels

    public static byte[] SerializeClass(ImageAnimation settings)
    {
        var ms = new MemoryStream();
        BinaryWriter writer = new BinaryWriter(ms);
        writer.Write(settings.ID);
        writer.Write(settings.filenameGraphic);
        writer.Write(settings.totalDuration);
        writer.Write(settings.fadeIn);
        writer.Write(settings.fadeInStartTime);
        writer.Write(settings.fadeInDuration);
        writer.Write(settings.fadeOut);
        writer.Write(settings.fadeOutStartTime);
        writer.Write(settings.fadeOutDuration);
        writer.Write(settings.rotateIn);
        writer.Write(settings.rotateInClockwise);
        writer.Write(settings.rotateInStartTime);
        writer.Write(settings.rotateInDuration);
        writer.Write(settings.rotationInDegrees);
        writer.Write(settings.rotateOut);
        writer.Write(settings.rotateOutClockwise);
        writer.Write(settings.rotateOutStartTime);
        writer.Write(settings.rotateOutDuration);
        writer.Write(settings.rotationOutDegrees);
        writer.Write(settings.zoomIn);
        writer.Write(settings.zoomInStartTime);
        writer.Write(settings.zoomInSpeed);
        writer.Write(settings.zoomInSize);
        writer.Write(settings.zoomOut);
        writer.Write(settings.zoomOutStartTime);
        writer.Write(settings.zoomOutSpeed);
        writer.Write(settings.zoomOutSize);
        return ms.ToArray();
    }

    public static ImageAnimation DeserializeClass(byte[] buffer)
    {
        var memStream = new MemoryStream(buffer);
        //var arr = buffer;
        ImageAnimation spinZoom = new ImageAnimation();
        BinaryReader reader = new BinaryReader(memStream);
        spinZoom.ID = reader.ReadInt32();
        spinZoom.filenameGraphic = reader.ReadString();
        spinZoom.totalDuration = reader.ReadSingle();
        spinZoom.fadeIn = reader.ReadBoolean();
        spinZoom.fadeInStartTime = reader.ReadSingle();
        spinZoom.fadeInDuration = reader.ReadSingle();
        spinZoom.fadeOut = reader.ReadBoolean();
        spinZoom.fadeOutStartTime = reader.ReadSingle();
        spinZoom.fadeOutDuration = reader.ReadSingle();
        spinZoom.rotateIn = reader.ReadBoolean();
        spinZoom.rotateInClockwise = reader.ReadBoolean();
        spinZoom.rotateInStartTime = reader.ReadSingle();
        spinZoom.rotateInDuration = reader.ReadSingle();
        spinZoom.rotationInDegrees = reader.ReadSingle();
        spinZoom.rotateOut = reader.ReadBoolean();
        spinZoom.rotateOutClockwise = reader.ReadBoolean();
        spinZoom.rotateOutStartTime = reader.ReadSingle();
        spinZoom.rotateOutDuration = reader.ReadSingle();
        spinZoom.rotationOutDegrees = reader.ReadSingle();
        spinZoom.zoomIn = reader.ReadBoolean();
        spinZoom.zoomInStartTime = reader.ReadSingle();
        spinZoom.zoomInSpeed = reader.ReadSingle();
        spinZoom.zoomInSize = reader.ReadSingle();
        spinZoom.zoomOut = reader.ReadBoolean();
        spinZoom.zoomOutStartTime = reader.ReadSingle();
        spinZoom.zoomOutSpeed = reader.ReadSingle();
        spinZoom.zoomOutSize = reader.ReadSingle();
        return spinZoom;
    }


}




[Serializable]
public enum TrayDirection
{
    Up = 0,
    Down,
    Left,
    Right
}



[Serializable]
public class MyTransform
{
    public MyVector3 position = new MyVector3();
    public MyQuaternion rotation = new MyQuaternion();
    public MyVector3 scale = new MyVector3();
    public int sortingOrder = 0;

    public Vector3 GetPositionAsVector3()
    {
        return new Vector3(position.x, position.y, position.z);
    }

    public Quaternion GetRotationAsQuaternion()
    {
        return new Quaternion(rotation.x, rotation.y, rotation.z, rotation.w);
    }

    public Vector3 GetScaleAsVector3()
    {
        return new Vector3(scale.x, scale.y, scale.z);
    }
}


[Serializable]
public class Tray
{
    public int trayID = -1;
    public string trayFilename = "";
    public string trayText = "";
    public UserColor trayTextColor = new UserColor(255, 255, 255);
    public bool trayUseCountdown = false;
    public int trayFadeInSpeed = 20;
    public int trayFadeOutSpeed = 20;
    public TrayDirection trayDirection = TrayDirection.Right;
    public int trayCountdownMinutes = 60;
    public bool trayUseTimer = false;
    public bool trayUseAutoAnimate = false;
    public int trayFadeInIntervalFromSeconds = 360;
    public int trayFadeInIntervalToSeconds = 360;
    public int trayFadeOutIntervalFromSeconds = 60;
    public int trayFadeOutIntervalToSeconds = 60;
    public MyTransform trayTransform = new MyTransform();

    public override string ToString()
    {
        return "ID: " + trayID +
               " Filename: " + trayFilename +
               " trayText: " + trayText +
               " trayTextColor: " + trayTextColor.r + " " + trayTextColor.g + " " + trayTextColor.b +
               " FadeIn: " + trayFadeInSpeed + " fadeout: " + trayFadeOutSpeed +
               " traydirection: " + trayDirection +
               " traycountdown: " + trayUseCountdown + " minutes: " + trayCountdownMinutes +
               " trayTimer: " + trayUseTimer +
               " trayAutoAnimate: " + trayUseAutoAnimate +
               " trayInterval FadeIn From: " + trayFadeInIntervalFromSeconds + " to: " + trayFadeInIntervalToSeconds +
               " trayInterval Fadeout From: " + trayFadeOutIntervalFromSeconds + " to: " + trayFadeOutIntervalToSeconds;
    }


    public static byte[] SerializeClass(Tray pTray)
    {
        MemoryStream ms = new MemoryStream();
        BinaryWriter writer = new BinaryWriter(ms);
        writer.Write(pTray.trayID);
        writer.Write(pTray.trayFilename);
        writer.Write(pTray.trayText);
        writer.Write(pTray.trayTextColor.r);
        writer.Write(pTray.trayTextColor.g);
        writer.Write(pTray.trayTextColor.b);
        writer.Write(pTray.trayFadeInSpeed);
        writer.Write(pTray.trayFadeOutSpeed);
        writer.Write((int) pTray.trayDirection);
        writer.Write(pTray.trayUseCountdown);
        writer.Write(pTray.trayCountdownMinutes);
        writer.Write(pTray.trayUseTimer);
        writer.Write(pTray.trayUseAutoAnimate);
        writer.Write(pTray.trayFadeInIntervalFromSeconds);
        writer.Write(pTray.trayFadeInIntervalToSeconds);
        writer.Write(pTray.trayFadeOutIntervalFromSeconds);
        writer.Write(pTray.trayFadeOutIntervalToSeconds);
        // mytransform is exempt from serialize and deserialize because data will not be transferred
        return ms.ToArray();
    }


    public static Tray DeserializeClass(byte[] buffer)
    {
        MemoryStream memStream = new MemoryStream(buffer);
        BinaryReader reader = new BinaryReader(memStream);
        Tray tray = new Tray();
        byte r;
        byte g;
        byte b;

        tray.trayID = reader.ReadInt32();
        tray.trayFilename = reader.ReadString();
        tray.trayText = reader.ReadString();

        r = reader.ReadByte();
        g = reader.ReadByte();
        b = reader.ReadByte();
        tray.trayTextColor = new UserColor(r, g, b);

        tray.trayFadeInSpeed = reader.ReadInt32();
        tray.trayFadeOutSpeed = reader.ReadInt32();
        tray.trayDirection = (TrayDirection) reader.ReadInt32();
        tray.trayUseCountdown = reader.ReadBoolean();
        tray.trayCountdownMinutes = reader.ReadInt32();
        tray.trayUseTimer = reader.ReadBoolean();
        tray.trayUseAutoAnimate = reader.ReadBoolean();
        tray.trayFadeInIntervalFromSeconds = reader.ReadInt32();
        tray.trayFadeInIntervalToSeconds = reader.ReadInt32();
        tray.trayFadeOutIntervalFromSeconds = reader.ReadInt32();
        tray.trayFadeOutIntervalToSeconds = reader.ReadInt32();

        return tray;
    }
}



[Serializable]
public class UserColor
{
    public UserColor() { }

    public UserColor(UserColor col)
    {
        r = col.r;
        g = col.g;
        b = col.b;
        a = col.a;
    }

    public UserColor(float red, float green, float blue, float alpha)
    {
        r = red;
        g = green;
        b = blue;
        a = alpha;
    }

    public UserColor(byte red, byte green, byte blue, byte alpha = 255)
    {
        r = red / 255.0f;
        g = green / 255.0f;
        b = blue / 255.0f;
        a = alpha / 255.0f;
    }

    public UserColor(Color pColor)
    {
        r = pColor.r;
        g = pColor.g;
        b = pColor.b;
        a = pColor.a;
    }

    public Color GetUnityColor()
    {
        
        Color color = new Color(r, g, b, 1.0f);
        return color;
    }

    public void HexColorToUnityColor(int red, int green, int blue)
    {
        r = red/255.0f;
        g = green/255.0f;
        b = blue/255.0f;
        a = 1.0f;
    }

    public float r = 0f;
    public float g = 0f;
    public float b = 0f;
    public float a = 0f;
}

[Serializable]
public class MyVector3
{
    public float x = 1.0f;
    public float y = 1.0f;
    public float z = 1.0f;

    public MyVector3(Vector3 vector3)
    {
        x = vector3.x;
        y = vector3.y;
        z = vector3.z;
    }

    public MyVector3(float pX, float pY, float pZ)
    {
        x = pX;
        y = pY;
        z = pZ;
    }

    public MyVector3()
    {
        x = 1.0f;
        y = 1.0f;
        z = 1.0f;
    }

}

[Serializable]
public class MyQuaternion
{
    public float x = 0.0f;
    public float y = 0.0f;
    public float z = 0.0f;
    public float w = 1.0f;

    public MyQuaternion() { }

    public MyQuaternion(Quaternion pQuaternion)
    {
        x = pQuaternion.x;
        y = pQuaternion.y;
        z = pQuaternion.z;
        w = pQuaternion.w;
    }

}

[Serializable]
public class MyGameObject
{
    public int ID = -1;
    public string ObjectFileName = "";
    public MyVector3 position = new MyVector3();
    public MyQuaternion rotation = new MyQuaternion();
    public MyVector3 scale = new MyVector3();
    public int sortingOrder = 0;

    public void SetPosition(Vector3 pPos)
    {
        position.x = pPos.x;
        position.y = pPos.y;
        position.z = pPos.z;
    }

    public void SetRotation(Quaternion pRot)
    {
        rotation.x = pRot.x;
        rotation.y = pRot.y;
        rotation.z = pRot.z;
        rotation.w = pRot.w;
    }

    public void SetScale(Vector3 pScale)
    {
        scale.x = pScale.x;
        scale.y = pScale.y;
        scale.z = pScale.z;
    }

    public void SetParameters(Vector3 pPos, Quaternion pRot, Vector3 pScale)
    {
        position.x = pPos.x;
        position.y = pPos.y;
        position.z = pPos.z;
        rotation.x = pRot.x;
        rotation.y = pRot.y;
        rotation.z = pRot.z;
        rotation.w = pRot.w;
        scale.x = pScale.x;
        scale.y = pScale.y;
        scale.z = pScale.z;
    }

    public Vector3 GetPositionAsVector3()
    {
        return new Vector3(position.x, position.y, position.z);
    }

    public Quaternion GetRotationAsQuaternion()
    {
        return new Quaternion(rotation.x, rotation.y, rotation.z, rotation.w);
    }

    public Vector3 GetScaleAsVector3()
    {
        return new Vector3(scale.x, scale.y, scale.z);
    }

}

public class ColorHSL
{
    public float H;
    public float S;
    public float L;

    public void ConvertToHSL(Color32 pCol)
    {
        //ColorHSL hsl = new ColorHSL();

        float r = pCol.r / 255.0f;
        float g = pCol.g / 255.0f;
        float b = pCol.b / 255.0f;
        float max = Mathf.Max(r, g);
        float min = Mathf.Min(r, g);
        max = Mathf.Max(max, b);
        min = Mathf.Min(min, b);

        float h, s;
        float l = (max + min) / 2;

        if (max == min)
        {
            H = 0;
            S = 0;
            L = l;
            return;
        }
        else
        {
            float delta = max - min;
            s = l > 0.5f ? delta / (2 - max - min) : delta / (max + min);
            if (max == r)
            {
                h = (g - b) / delta + (g < b ? 6 : 0);
            }
            else if (max == g)
            {
                h = (b - r) / delta + 2;

            }
            else // (max == b)
            {
                h = (r - g) / delta + 4;
            }
            h /= 6;
        }

        H = h * 360;
        S = s * 100;
        L = l * 100;
    }

    public static float GetHue(Color32 pCol)
    {
        float r = pCol.r / 255.0f;
        float g = pCol.g / 255.0f;
        float b = pCol.b / 255.0f;
        float max = Mathf.Max(r, g);
        float min = Mathf.Min(r, g);
        max = Mathf.Max(max, b);
        min = Mathf.Min(min, b);

        float h;
        //float l = (max + min) / 2;

        if (max == min)
        {
            h = 0;
            //s = 0;
            return h;
        }
        else
        {
            float delta = max - min;
            //s = l > 0.5f ? delta / (2 - max - min) : delta / (max + min);
            if (max == r)
            {
                h = (g - b) / delta + (g < b ? 6 : 0);
            }
            else if (max == g)
            {
                h = (b - r) / delta + 2;

            }
            else // (max == b)
            {
                h = (r - g) / delta + 4;
            }
            h /= 6;
        }

        return h*360;
    }

}



// graveyard SpinZoomPackage
//public bool fadeInAnimation = true; // should it fade in at start?
//public bool fadeOutAnimation = true; // Should it fade out at the end?
//public float startFadeInTime = 1.0f; // When in the animation should the fadein start.
////public float fadeInDuration = 1.0f; // How long time the fadeIn duration should take.
//public float fadeOutTime = 8.0f; // When the animation should fade out

//public bool rotateInAnimation = true; // should the animation rotate in?
//public bool rotateOutAnimation = true;  // should the animation rotate out ?
//public bool rotateEntireDuration = true; // Do you want the animation to rotate all the time? If yes, then other choices are moot
//public bool rotateClockwise = true;  // do you want the rotation to happen clockwise or couter clockwise?
//public bool rotateClockwiseOut = true;
//public float startRotationInTime = 0.0f; // When should the animation start rotate?
//public float rotateInDuration = 7.0f; // what is the duration of the in rotation?
//public float totalDegreesToRotateIn = 720; // How many degrees in total should the animation rotate?
//public float rotateOutTime = 8.0f; // When should the animation start rotating out?
//public float totalDegreesToRotateOut = 720;

//public bool zoomInZoomOut = true; // 
//public bool zoomOutZoomIn = false;
//public float startZoomInAnimation = 0.0f;  // When should the animation zoom in?
//public float zoomInAnimationTime = 7.0f; // When should the animation start zooming out?
//public float zoomOutTime = 8.0f; // At what time should it start zooming out?
//public float zoomToTarget = 1.5f;  // what is the zoom target, how big should it be?
//public float zoomFromTarget = 0.0f; // What is the starting zoom size?
//public float zoomInAnimationDuration = 4.0f; // How long should the zoom in last
//public float zoomOutAnimationDuration = 1.0f;



//public string GetKey(TrayKeys key)
//{
//    switch (key)
//    {
//        case TrayKeys.Identifier:
//            return "<ID:" + trayID + ">";
//        //break;
//        case TrayKeys.Filename:
//            return "<Filename:" + trayFilename + ">";
//        case TrayKeys.Text:
//            return "<Text:" + trayText + ">";
//        case TrayKeys.TextColor:
//            return "<TextColor: R(" + trayTextColor.R + ")G(" + trayTextColor.G + ")B(" + trayTextColor.B + ")>";
//        case TrayKeys.FadeIn:
//            return "<FadeIn:" + trayFadeInSpeed + ">";
//        case TrayKeys.FadeOut:
//            return "<FadeOut:" + trayFadeOutSpeed + ">";
//        case TrayKeys.Direction:
//            return "<Direction:" + trayDirection.ToString() + ">";
//        case TrayKeys.CountdownBool:
//            return "<UseCooldown:" + trayUseCountdown + ">";
//        case TrayKeys.CountdownTime:
//            return "<Countdown:" + trayCountdownMinutes + ">";
//        case TrayKeys.UseTimer:
//            return "<UseTimer:" + trayUseTimer + ">";
//        case TrayKeys.AutoAnimateBool:
//            return "<UseAutoAnimate:" + trayUseAutoAnimate + ">";
//        case TrayKeys.AutoFadeInAnimateFrom:
//            return "<FadeInIntervalFrom:" + trayFadeInIntervalFromSeconds + ">";
//        case TrayKeys.AutoFadeInAnimateTo:
//            return "<FadeInIntervalTo: " + trayFadeInIntervalToSeconds + ">";
//        case TrayKeys.AutoFadeOutAnimateFrom:
//            return "<FadeOutIntervalFrom:" + trayFadeOutIntervalFromSeconds + ">";
//        case TrayKeys.AutoFadeOutAnimateTo:
//            return "<FadeOutIntervalTo:" + trayFadeOutIntervalToSeconds + ">";
//        case TrayKeys.All:

//            return "<ID:" + trayID + ">" +
//                   "<Filename:" + trayFilename + ">" +
//                   "<Text:" + trayText + ">" +
//                   "<TextColor: R(" + trayTextColor.R + ")G(" + trayTextColor.G + ")B(" + trayTextColor.B + ")>" +
//                   "<FadeIn:" + trayFadeInSpeed + ">" +
//                   "<FadeOut:" + trayFadeOutSpeed + ">" +
//                   "<Direction:" + trayDirection.ToString() + ">" +
//                   "<UseCountdown:" + trayUseCountdown + ">" +
//                   "<Countdown:" + trayCountdownMinutes + ">" +
//                   "<UseTimer:" + trayUseTimer + ">" +
//                   "<UseAutoAnimate:" + trayUseAutoAnimate + ">" +
//                   "<FadeInIntervalFrom:" + trayFadeInIntervalFromSeconds + ">" +
//                   "<FadeInIntervalTo: " + trayFadeInIntervalToSeconds + ">" +
//                   "<FadeOutIntervalFrom:" + trayFadeOutIntervalFromSeconds + ">" +
//                   "<FadeOutIntervalTo: " + trayFadeOutIntervalToSeconds + ">";
//        default:
//            return "Not a valid key";

//    }

//}


//[Serializable]
//public class Preset
//{
//    public string presetName;
//    public bool selectedPreset = false;
//    public List<MyGameObject> gameImages = new List<MyGameObject>();
//    public List<Tray> listOfTrays = new List<Tray>();
//    public bool useTickerText = true;
//    public bool useTickerBackground = true;
//    public string tickerText = "";
//    public float tickerSpeed = 0.5f;
//    public float tickerRepeatTime = 35.0f;
//    public UserColor backgroundColor = new UserColor();
//    public UserColor tickerBackgroundColor = new UserColor();
//    public UserColor tickerTextColor =  new UserColor();
//    public string backgroundGraphicFilename = "";

//    public void SetPreset(Preset pPreset)
//    {
//        presetName = pPreset.presetName;
//        for (int i = 0; i < pPreset.gameImages.Count; i++)
//        {
//            MyGameObject myObj = new MyGameObject();
//            myObj.ID = i;
//            myObj.ObjectFileName = pPreset.gameImages[i];
//            myObj.position = new MyVector3();
//            myObj.rotation = new MyQuaternion();
//            myObj.scale = new MyVector3();
//            gameImages.Add(myObj);
//        }

//        for (int i = 0; i < pPreset.listOfTrays.Count; i++)
//        {
//            Tray tray = new Tray();
//            tray.trayID = i;
//            tray.trayFilename = pPreset.listOfTrays[i].trayFilename;
//            tray = pPreset.listOfTrays[i];
//            tray.trayTransform.position = new MyVector3();
//            tray.trayTransform.rotation = new MyQuaternion();
//            tray.trayTransform.scale = new MyVector3();
//        }
//        backgroundColor = pPreset.backgroundColor;
//        tickerBackgroundColor = pPreset.tickerBackgroundColor;
//        tickerTextColor = pPreset.tickerTextColor;
//        tickerText = pPreset.tickerText;
//        tickerSpeed = pPreset.tickerSpeed;
//        tickerRepeatTime = pPreset.tickerRepeatTime;
//        backgroundGraphicFilename = "";
//    }
//}


//[Serializable]
//public class Tray
//{
//    public int ID = -1;
//    public string filename = "";
//    public string trayText = "";
//    public UserColor trayColor = new UserColor();
//    public bool useCountdown = false;
//    public int fadeInSpeed = -1;
//    public int fadeOutSpeed = -1;
//    public TrayDirection trayDirection = TrayDirection.Right;
//    public int counter = -1;
//    public bool useTimer = false;
//    public bool useAutoAnimate = false;
//    public int autoAnimateFadeInFromTime = -1;
//    public int autoAnimateFadeInToTime = -1;
//    public int autoAnimateFadeOutFromTime = -1;
//    public int autoAnimateFadeOutToTime = -1;

//    public override string ToString()
//    {

//        return "trayText: " + trayText + " ID: " + ID + " filename: " + filename + " useCounter: " + useCountdown +
//               " counter: " + counter + " Fadein: " + fadeInSpeed + " FadeOut: " + fadeOutSpeed + " userColor: r:" +
//               trayColor.r + " g:" + trayColor.g + " b:" + trayColor.b +
//               " use timer: " + useTimer + " useautoanimate: " + useAutoAnimate + 
//               " Direction: " + trayDirection +
//               " autoAnimateFadeInFromTime: " + autoAnimateFadeInFromTime +
//               " autoAnimateFadeInToTime: " + autoAnimateFadeInToTime +
//               " autoAnimateFadeOutFromTime: " + autoAnimateFadeOutFromTime +
//               " autoAnimateFadeOutToTime: " + autoAnimateFadeOutToTime;
//    }

//}