﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;
using Debug = UnityEngine.Debug;
//using EventHandler = System.EventHandler;

#pragma warning disable 0219

public class PackageHandler : MonoBehaviour
{
    //public GameObject imageGameObject;
    
    void Awake()
    {
        TwitchOverlay.EventHandler.OnReceivedPackage += new TwitchOverlay.EventHandler.SingleArgDelegate((package) => HandlePackage((PipePackage)package) );
    }

    void OnApplicationQuit()
    {
        //EventHandler.OnReceivedPackage -= new EventHandler.SingleArgDelegate((package) => HandlePackage((PipePackage)package));
    }


	// Use this for initialization
	void Start () {
	
	}


    private void HandlePackage(PipePackage package)
    {
        
        switch (package.type)
        {
            case PipePackage.Packagetype.Animation:
            {
                    
                ImageAnimation spinZoom = ImageAnimation.DeserializeClass(package.buffer);
                TwitchOverlay.EventHandler.AddAnimatedImage(spinZoom);

                break;
            }
            case PipePackage.Packagetype.Background:
            {
                    /* this gives an error: get_main can only be called from the main thread.
                    Constructors and field initializers will be executed from the loading thread when loading a scene.
                    Don't use this function in the constructor or field initializers, instead move initialization code to the Awake or Start function.*/
                    // The work around is to have the camera listen for an event.
                    //Debug.Log("Length of buffer received is: " + package.buffer.Length);
                    //Debug.Log((int)package.buffer[0] + " " + (int)package.buffer[1] + " " + (int)package.buffer[2]);

                    Color bgColor = new Color();
                    bgColor.r = (float)package.buffer[0] / 255;
                    bgColor.g = (float)package.buffer[1] / 255;
                    bgColor.b = (float)package.buffer[2] / 255;
                    bgColor.a = 1.0f;

                    TwitchOverlay.EventHandler.ChangeBgColor(bgColor);

                    //Camera.main.backgroundColor = bgColor;
                    Debug.Log("BG info received");
                break;
            }
            case PipePackage.Packagetype.Information:
            {
                    //Debug.Log("Length of buffer received is: " + package.buffer.Length);
                    //Debug.Log(package.clearText);
                MemoryStream ms = new MemoryStream(package.buffer);
                BinaryReader reader = new BinaryReader(ms);
                float speed = reader.ReadSingle();
                float repeatTime = reader.ReadSingle();
                string tickerText = reader.ReadString();
                TwitchOverlay.EventHandler.ReceivedSpeedAndDuration(speed, repeatTime);
                TwitchOverlay.EventHandler.UpdateTickerText(tickerText);

                    break;
            }
            case PipePackage.Packagetype.Layout:
            {

                break;
            }
            case  PipePackage.Packagetype.NotAValidPackage:
            {

                break;
            }
            case PipePackage.Packagetype.Quit:
            {
                //Application.Quit();
                break;
            }

            case PipePackage.Packagetype.FadeOutTicker:
            {
                TwitchOverlay.EventHandler.FadeOutInterfaceTrigger();
                break;
            }

            case PipePackage.Packagetype.TickerColor:
            {
                    Color bgColor = new Color();
                    bgColor.r = (float)package.buffer[0] / 255;
                    bgColor.g = (float)package.buffer[1] / 255;
                    bgColor.b = (float)package.buffer[2] / 255;
                    bgColor.a = 1.0f;

                    TwitchOverlay.EventHandler.UpdateTickerTextColor(bgColor);
                    break;
            }
            case PipePackage.Packagetype.TickerBackgroundColor:
            {

                    Color bgColor = new Color();
                    bgColor.r = (float)package.buffer[0] / 255;
                    bgColor.g = (float)package.buffer[1] / 255;
                    bgColor.b = (float)package.buffer[2] / 255;
                    bgColor.a = 1.0f;
                    TwitchOverlay.EventHandler.UpdateTickerBackgroundColor(bgColor);
                    break;
            }

            case PipePackage.Packagetype.TickerTextOnOff:
            {
                TwitchOverlay.EventHandler.TurnOnOffTicker();
                break;
            }

            case PipePackage.Packagetype.ImageFile:
            {
                HandleImageFile(package);
                break;
            }
            
            case PipePackage.Packagetype.ImageLocation:
            {
                HandleAddImage(package);
                break;
            }
            case PipePackage.Packagetype.WebcamChromaKey:
            {
                    Color32 chromaKey = new Color32(package.buffer[0], package.buffer[1], package.buffer[2], 255);
                    TwitchOverlay.EventHandler.ChangeWebCamChoromaKey(chromaKey);
                    Debug.Log("Chroma key" + package.clearText);
                    break;
            }
            case PipePackage.Packagetype.AddTray:
            {
                Debug.Log("Got a change text request: " + package.clearText);
                TrayIconAdd(package);   
                
                break;
            }
            case PipePackage.Packagetype.TrayOnOff:
            {
                string idKey = "<ID:";
                string endingKeystring = ">";
                string packageString = package.clearText;
                int keyStart = packageString.IndexOf(idKey) + idKey.Length;
                int keyEnd = packageString.IndexOf(endingKeystring);
                string tempString = packageString.Substring(keyStart, keyEnd - keyStart);
                int ID = int.Parse(tempString);
                Debug.Log("ID: " + ID);

                TwitchOverlay.EventHandler.TurnTrayOnOff(ID);
                break;
            }

            case PipePackage.Packagetype.TrayAutoAnimate:
            {
                int automateID = int.Parse(package.clearText);
                TwitchOverlay.EventHandler.TurnOnAutoAnimation(automateID);
                //Debug.Log("ID gotten and autoanimated: " + automateID);

                break;
            }
            case PipePackage.Packagetype.Preset:
            {
                HandlePreset(package);
                break;
            }


            default:
            {
                break;
            }
        }

    }

    private void HandleImageFile(PipePackage package)
    {
        TwitchOverlay.EventHandler.ChangeImage(package.clearText);
    }

    private void HandleAddImage(PipePackage package)
    {
        TwitchOverlay.EventHandler.AddImageToScene(package.clearText);
    }

    // TODO: needs a rewrite 
    private void TrayIconAdd(PipePackage package)
    {
        Tray tray = new Tray();
        tray = Tray.DeserializeClass(package.buffer);
        tray.trayTransform.position = new MyVector3();
        tray.trayTransform.rotation = new MyQuaternion();
        tray.trayTransform.scale = new MyVector3(1.0f, 1.0f, 1.0f);

        TwitchOverlay.EventHandler.AddTrayToScene(tray);
    }



    private void HandlePreset(PipePackage pPackage)
    {
        Preset preset = Preset.DeserializeClass(pPackage.buffer);
        Debug.Log("Preset received: " + preset.presetName + " TrayCount: " + preset.listOfTrays.Count + " preset gameimages: " + preset.gameImages.Count);
        TwitchOverlay.EventHandler.ReceivedPreset(preset);

    }




}

// graveyard

// old handle image:
//if (package.clearText.Contains("Open Sesame"))
//{
//    bool success = false;
//    int startIndex = package.clearText.IndexOf(":") + 1;
//    int length = package.clearText.Length - startIndex;

//    string filename = package.clearText.Substring(startIndex, length);
//    Debug.Log("Filename: " + filename);
//    // open a socket and begin listening, then close it as soon as it is done and encode the file.
//    // An added security feature could be to randomize port and send that information back, but we just want the file transferred to this client and then close the connection

//    int port = 63002;
//    IPAddress myIP = IPAddress.Parse("127.0.0.1");
//    TcpListener listener = new TcpListener(myIP, port);
//    listener.Start();
//    listener.BeginAcceptTcpClient(OnConnected, listener);
//    TcpClient client = listener.AcceptTcpClient();
//    NetworkStream stream = client.GetStream();

//    int bytesRead = 0;
//    byte[] buffer = new byte[1024];
//    if (client.Connected)
//    {
//        FileStream output = File.Create(".\\" + filename);
//        while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
//        {
//            output.Write(buffer, 0, bytesRead);
//            output.Flush();
//            success = true;
//        }
//        output.Dispose();

//    }
//    listener.Stop();
//    client.Close();
//    if (success)
//    {
//        TwitchOverlay.EventHandler.ChangeImage(filename);
//    }
//}



//int keyStart = packageString.IndexOf(idKey) + idKey.Length;
//int keyEnd = packageString.IndexOf(endingKeystring);
//string tempString = packageString.Substring(keyStart, keyEnd - keyStart);
//keyStart = packageString.IndexOf(filenameKey) + filenameKey.Length;
//keyEnd = packageString.IndexOf(endingKeystring, keyEnd + 1);
//string tempString = "";
//keyStart = packageString.IndexOf(textKey) + textKey.Length;
//keyEnd = packageString.IndexOf(endingKeystring, keyEnd + 1);
//tempString = packageString.Substring(keyStart, keyEnd - keyStart);
//keyStart = packageString.IndexOf(useCooldownKey) + useCooldownKey.Length;
//keyEnd = packageString.IndexOf(endingKeystring, keyEnd + 1);
//tempString = packageString.Substring(keyStart, keyEnd - keyStart);
//keyStart = packageString.IndexOf(cooldownKey) + cooldownKey.Length;
//keyEnd = packageString.IndexOf(endingKeystring, keyEnd + 1);
//tempString = packageString.Substring(keyStart, keyEnd - keyStart);
//keyStart = packageString.IndexOf(fadeInKey) + fadeInKey.Length;
//keyEnd = packageString.IndexOf(endingKeystring, keyEnd + 1);
//tempString = packageString.Substring(keyStart, keyEnd - keyStart);



//Debug.Log(package.buffer.Length);

//                var memStream = new MemoryStream(package.buffer);
//var arr = package.buffer;
//Debug.Log(arr.Length);
//                BinaryReader reader = new BinaryReader(memStream);
//spinZoom.ID = reader.ReadInt32();
//                spinZoom.filenameGraphic = reader.ReadString();
//                spinZoom.totalDuration = reader.ReadSingle();
//                spinZoom.fadeIn = reader.ReadBoolean();
//                spinZoom.fadeInStartTime = reader.ReadSingle();
//                spinZoom.fadeInDuration = reader.ReadSingle();
//                spinZoom.fadeOut = reader.ReadBoolean();
//                spinZoom.fadeOutStartTime = reader.ReadSingle();
//                spinZoom.fadeOutDuration = reader.ReadSingle();
//                spinZoom.rotateIn = reader.ReadBoolean();
//                spinZoom.rotateInClockwise = reader.ReadBoolean();
//                spinZoom.rotateInStartTime = reader.ReadSingle();
//                spinZoom.rotateInDuration = reader.ReadSingle();
//                spinZoom.rotationInDegrees = reader.ReadSingle();
//                spinZoom.rotateOut = reader.ReadBoolean();
//                spinZoom.rotateOutClockwise = reader.ReadBoolean();
//                spinZoom.rotateOutStartTime = reader.ReadSingle();
//                spinZoom.rotateOutDuration = reader.ReadSingle();
//                spinZoom.rotationOutDegrees = reader.ReadSingle();
//                spinZoom.zoomIn = reader.ReadBoolean();
//                spinZoom.zoomInStartTime = reader.ReadSingle();
//                spinZoom.zoomInSpeed = reader.ReadSingle();
//                spinZoom.zoomInSize = reader.ReadSingle();
//                spinZoom.zoomOut = reader.ReadBoolean();
//                spinZoom.zoomOutStartTime = reader.ReadSingle();
//                spinZoom.zoomOutSpeed = reader.ReadSingle();
//                spinZoom.zoomOutSize = reader.ReadSingle();

//                    //Debug.Log(spinZoom.ID                );
//                    //Debug.Log(spinZoom.filenameGraphic   );
//                    //Debug.Log(spinZoom.totalDuration     );
//                    //Debug.Log(spinZoom.fadeIn            );
//                    //Debug.Log(spinZoom.fadeInStartTime   );
//                    //Debug.Log(spinZoom.fadeInDuration    );
//                    //Debug.Log(spinZoom.fadeOut           );
//                    //Debug.Log(spinZoom.fadeOutStartTime  );
//                    //Debug.Log(spinZoom.fadeOutDuration   );
//                    //Debug.Log(spinZoom.rotateIn          );
//                    //Debug.Log(spinZoom.rotateInClockwise );
//                    //Debug.Log(spinZoom.rotateInStartTime );
//                    //Debug.Log(spinZoom.rotateInDuration  );
//                    //Debug.Log(spinZoom.rotationInDegrees );
//                    //Debug.Log(spinZoom.rotateOut         );
//                    //Debug.Log(spinZoom.rotateOutClockwise);
//                    //Debug.Log(spinZoom.rotateOutStartTime);
//                    //Debug.Log(spinZoom.rotateOutDuration );
//                    //Debug.Log(spinZoom.rotationOutDegrees);
//                    //Debug.Log(spinZoom.zoomIn);
//                    //Debug.Log(spinZoom.zoomInStartTime);
//                    //Debug.Log(spinZoom.zoomInSpeed);
//                    //Debug.Log(spinZoom.zoomInSize);
//                    //Debug.Log(spinZoom.zoomOut);
//                    //Debug.Log(spinZoom.zoomOutStartTime );
//                    //Debug.Log(spinZoom.zoomOutSpeed);
//                    //Debug.Log(spinZoom.zoomOutSize);


// Add Tray
//Tray Tray = new Tray();
//string idKey                    = "<ID:";
//string filenameKey              = "<Filename:";
//string textKey                  = "<Text:";
//string fadeInKey                = "<FadeIn:";
//string fadeOutKey               = "<FadeOut:";
//string useCountdownKey          = "<UseCountdown:";
//string countdownKey             = "<Countdown:";
//string textColorKey             = "<TextColor:";
//string directionKey             = "<Direction:";
//string useTimerKey              = "<UseTimer:";
//string useAutoAnimateKey        = "<UseAutoAnimate:";
//string intervalFadeInFromKey    = "<FadeInIntervalFrom:";
//string intervalFadeInToKey      = "<FadeInIntervalTo:";
//string intervalFadeOutFromKey   = "<FadeOutIntervalFrom:";
//string intervalFadeOutToKey     = "<FadeOutIntervalTo:";
//string rKey                     = "R(";
//string gKey                     = "G(";
//string bKey                     = "B(";
//string colorEndKey              = ")";
//string endingKeystring          = ">";
//string packageString = package.clearText;
//int keyStart = 0;
//int keyEnd = 0;

//// get the ID
//string tempString = FindKey(packageString, idKey, endingKeystring, ref keyStart, ref keyEnd);
//Debug.Log("ID start: " + keyStart + " ID end: " + keyEnd + " tempstring: " + tempString);
//int id = int.Parse(tempString);
//Tray.ID = id;
//Debug.Log("ID: " + id);

//// Get the filename to load
//tempString = FindKey(packageString, filenameKey, endingKeystring, ref keyStart, ref keyEnd);
//Debug.Log("Filename start: " + keyStart + " Filename end: " + keyEnd + " tempstring: " + tempString);
//Tray.filename = tempString;

//// get the text to display
//tempString = FindKey(packageString, textKey, endingKeystring, ref keyStart, ref keyEnd);
//tempString = tempString.Replace('|', '\n');
//Tray.trayText = tempString;
//Debug.Log("Text start: " + keyStart + " Text end: " + keyEnd + " textstring: " + tempString);

//// text color. Use the hexcolor conversion. Note this is not sent as bytes, but it shouldn't matter
//tempString = FindKey(packageString, rKey, colorEndKey, ref keyStart, ref keyEnd);
//int r = int.Parse(tempString);
//tempString = FindKey(packageString, gKey, colorEndKey, ref keyStart, ref keyEnd);
//int g = int.Parse(tempString);
//tempString = FindKey(packageString, bKey, colorEndKey, ref keyStart, ref keyEnd);
//int b = int.Parse(tempString);
//UserColor color = new UserColor();
//color.HexColorToUnityColor(r, g, b);
//Tray.trayColor = color;

//// Fade in speed
//tempString = FindKey(packageString, fadeInKey, endingKeystring, ref keyStart, ref keyEnd);
//if (!string.IsNullOrEmpty(tempString))
//{
//    Tray.fadeInSpeed = int.Parse(tempString);
//}
//else
//{
//    Tray.fadeInSpeed = 20;
//}

////fade out speed
//tempString = FindKey(packageString, fadeOutKey, endingKeystring, ref keyStart, ref keyEnd);
//if (!string.IsNullOrEmpty(tempString))
//{
//    Tray.fadeOutSpeed = int.Parse(tempString);
//}
//else
//{
//    Tray.fadeOutSpeed = 20;
//}

//// direction

//tempString = FindKey(packageString, directionKey, endingKeystring, ref keyStart, ref keyEnd);
//if (tempString == "Left")
//{
//    Tray.trayDirection = TrayDirection.Left;
//} else if (tempString == "Up")
//{
//    Tray.trayDirection = TrayDirection.Up;
//} else if (tempString == "Down")
//{
//    Tray.trayDirection = TrayDirection.Down;
//}
//else
//{
//    Tray.trayDirection = TrayDirection.Right;
//}

//// get the cooldown bool
//tempString = FindKey(packageString, useCountdownKey, endingKeystring, ref keyStart, ref keyEnd);

//tempString = tempString.Trim();
//if (tempString.Contains("True") || tempString.Contains("true"))
//{
//    Tray.useCountdown = true;
//}
//else
//{
//    Tray.useCountdown = false;
//}
//Debug.Log("UseCooldown start: " + keyStart + " UseCooldown end: " + keyEnd + " UseCooldown: " + Tray.useCountdown);

//// get the cooldown timer
//tempString = FindKey(packageString, countdownKey, endingKeystring, ref keyStart, ref keyEnd);
//// should not get any text, so removed try block.
//if (!string.IsNullOrEmpty(tempString))
//{
//    Tray.counter = int.Parse(tempString);
//}
//else
//{
//    Tray.counter = 0;
//}

//// get the useTimer 
//tempString = FindKey(packageString, useTimerKey, endingKeystring, ref keyStart, ref keyEnd);
//if (!string.IsNullOrEmpty(tempString))
//{
//    if (tempString == "True" || tempString == "true")
//    {
//        Tray.useTimer = true;
//    }
//    else
//    {
//        Tray.useTimer = false;
//    }
//}

//// auto animate
//tempString = FindKey(packageString, useAutoAnimateKey, endingKeystring, ref keyStart, ref keyEnd);
//if (!string.IsNullOrEmpty(tempString))
//{
//    if (tempString == "True" || tempString == "true")
//    {
//        Tray.useAutoAnimate = true;
//    }
//    else
//    {
//        Tray.useAutoAnimate = false;
//    }
//}

//// auto animate from 
//tempString = FindKey(packageString, intervalFadeInFromKey, endingKeystring, ref keyStart, ref keyEnd);
//if (!string.IsNullOrEmpty(tempString))
//{
//    Tray.autoAnimateFadeInFromTime = int.Parse(tempString);
//}
//else
//{
//    Tray.autoAnimateFadeInFromTime = 10;
//}

//tempString = FindKey(packageString, intervalFadeInToKey, endingKeystring, ref keyStart, ref keyEnd);
//if (!string.IsNullOrEmpty(tempString))
//{
//    Tray.autoAnimateFadeInToTime = int.Parse(tempString);
//}
//else
//{
//    Tray.autoAnimateFadeInToTime = 10;
//}

//tempString = FindKey(packageString, intervalFadeOutFromKey, endingKeystring, ref keyStart, ref keyEnd);
//if (!string.IsNullOrEmpty(tempString))
//{
//    Tray.autoAnimateFadeOutFromTime = int.Parse(tempString);
//}
//else
//{
//    Tray.autoAnimateFadeOutFromTime = 10;
//}

//tempString = FindKey(packageString, intervalFadeOutToKey, endingKeystring, ref keyStart, ref keyEnd);
//if (!string.IsNullOrEmpty(tempString))
//{
//    Tray.autoAnimateFadeOutToTime = int.Parse(tempString);
//}
//else
//{
//    Tray.autoAnimateFadeOutToTime = 10;
//}

//Debug.Log(Tray.ToString());

//private string FindKey(string keyedString , string startKey, string endkey, ref int startKeyIndex, ref int endKeyIndex)
//{
//    startKeyIndex = keyedString.IndexOf(startKey) + startKey.Length;
//    endKeyIndex = keyedString.IndexOf(endkey, startKeyIndex); // used to be endkey.
//    string keyValue = keyedString.Substring(startKeyIndex, endKeyIndex - startKeyIndex);
//    return keyValue;
//}