﻿using UnityEngine;
using System.Collections;

public class RotationAnimationScript : MonoBehaviour
{

    private bool rotateIn = false;
    private bool rotateInCW = true;
    private float rotateInCounter = 2.0f;
    private float rotateInSteps = 1.0f;
    private float rotateInDegrees = 0;

    private bool rotateOut = false;
    private bool rotateOutCW = true;
    private float rotateOutCounter = 2.0f;
    private float rotateOutSteps = 1.0f;
    private float rotateOutDegrees = 0.0f;

    void Update()
    {
        if (rotateIn)
        {
            rotateInCounter -= Time.deltaTime;
            if (rotateInCounter > 0.0f)
            {
                if (rotateInCW)
                {
                    Vector3 rot = transform.rotation.eulerAngles;
                    rot.z -= Time.deltaTime*rotateInSteps;
                    transform.rotation = Quaternion.Euler(rot);
                }
                else
                {
                    Vector3 rot = transform.rotation.eulerAngles;
                    rot.z += Time.deltaTime*rotateInSteps;
                    transform.rotation = Quaternion.Euler(rot);
                }
            }
            else
            {
                Vector3 rot = transform.rotation.eulerAngles;
                rot.z = rotateInDegrees;
                transform.rotation = Quaternion.Euler(rot);
                rotateIn = false;
            }
        }

        if (rotateOut)
        {
            rotateOutCounter -= Time.deltaTime;
            if (rotateOutCounter > 0.0f)
            {
                if (rotateOutCW)
                {
                    Vector3 rot = transform.rotation.eulerAngles;
                    rot.z -= Time.deltaTime*rotateOutSteps;
                    transform.rotation = Quaternion.Euler(rot);
                }
                else
                {
                    Vector3 rot = transform.rotation.eulerAngles;
                    rot.z += Time.deltaTime*rotateOutSteps;
                    transform.rotation = Quaternion.Euler(rot);

                }
            }
            else
            {
                Vector3 rot = transform.rotation.eulerAngles;
                rot.z = rotateOutDegrees;
                transform.rotation = Quaternion.Euler(rot);
                rotateOut = false;
            }

        }
            
    }

    public void StartRotateIn(float pDegrees, float pDuration, bool pCW, bool pRotateIn)
    {
        rotateIn = pRotateIn;
        if (!(Mathf.Abs(pDegrees) > 0.0f) || !pRotateIn)
        {
            rotateIn = false;
            return;
        }
        rotateInCounter = pDuration;
        rotateInSteps = pDegrees/pDuration;
        rotateInCW = pCW;
        rotateInDegrees = pDegrees;
    }

    public void StartRotateOut(float pDegrees, float pDuration, bool pCW, bool pRotateOut)
    {
        rotateOut = pRotateOut;
        if (!(Mathf.Abs(pDegrees) > 0.0f) || !pRotateOut)
        {
            rotateOut = false;
            return;
        }
        rotateOutCounter = pDuration;
        rotateOutSteps = pDegrees/pDuration;
        rotateOutCW = pCW;
        rotateOutDegrees = pDegrees;
    }


}
