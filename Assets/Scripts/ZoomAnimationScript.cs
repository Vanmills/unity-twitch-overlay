﻿using UnityEngine;
using System.Collections;

public class ZoomAnimationScript : MonoBehaviour
{

    // zoom in
    private bool zoomIn = false; // Do you want to zoom the animation in?
    private float zoomInSize = 1.35f;
    //private float zoomInSteps = 0.0f;
    private float zoomInSpeed = 1.0f;


    // Zoom out
    private bool zoomOut = false; // Do you want the animation to zoom out?
    private float zoomOutSize = 0.0f;
    private float zoomOutSpeed = 1.0f;
    //private float zoomOutSteps = 0.0f;
    private bool zoomInDone = false;
    private bool zoomOutDone = false;
    // Update is called once per frame
    void Update () {

        if (zoomIn)
        {
            transform.localScale = Vector3.Lerp(transform.localScale,
                new Vector3(zoomInSize, zoomInSize, zoomInSize),
                zoomInSpeed * Time.deltaTime);
            if (zoomInSize - transform.localScale.x < 0.005f)
            {
                zoomOutDone = true;
                zoomIn = false;
                transform.localScale = new Vector3(zoomInSize, zoomInSize, zoomInSize);
            }
        }  else if (zoomOut)
        {
                transform.localScale = Vector3.Lerp(transform.localScale,
                    new Vector3(zoomOutSize, zoomOutSize, zoomOutSize),
                    zoomOutSpeed * Time.deltaTime);
            if (transform.localScale.x - zoomOutSize < 0.005f)
            {
                zoomOut = false;
                zoomOutDone = true;
                transform.localScale = new Vector3(zoomOutSize, zoomOutSize, zoomOutSize);
                
            }
        }
        else if (!zoomIn && !zoomOut && !zoomInDone && !zoomOutDone)
        {
            transform.localScale = new Vector3(zoomInSize, zoomInSize, zoomInSize);
        }

    }

    public void StartZoomIn(float pTargetSize, float pZoomInSpeed, bool pZoomIn = true)
    {
        zoomInSize = pTargetSize / 100.0f;
        zoomIn = pZoomIn;
        zoomOut = false;
        zoomInSpeed = pZoomInSpeed / 50.0f;
        
        if (pTargetSize < 0.0f)
        {
            transform.localScale = Vector3.zero;
        }
        if (pTargetSize <= 0.0f)
        {
            pTargetSize = 0.1f;
        }
        pTargetSize /= 100.0f;
        if (!zoomIn && pTargetSize > 0.0f)
        {
            transform.localScale = new Vector3(pTargetSize, pTargetSize, pTargetSize);
        }
        else
        {
            transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        }

    }

    public void StartZoomOut(float pTargetSize, float pZoomOutSpeed , bool pZoomOut = true)
    {
        zoomOutSize = pTargetSize / 100.0f;
        zoomOut = pZoomOut;
        zoomOutSpeed = pZoomOutSpeed / 50.0f;
        if (!zoomOut)
        {
            transform.localScale = new Vector3(pTargetSize, pTargetSize, pTargetSize);
        }
        zoomIn = false;
    }

}
