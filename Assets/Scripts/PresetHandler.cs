﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System;

public class PresetHandler : MonoBehaviour
{
    private Preset currentPreset;
    private bool dirty = false;
    List<Preset> loadedPresets = new List<Preset>();
    private string presetFilename = "unity-preset.xml";
    private bool usingPreset = false;

    void Awake()
    {
        TwitchOverlay.EventHandler.OnReceivedPreset += new TwitchOverlay.EventHandler.SingleArgDelegate((pPreset)=>SetPreset((Preset)pPreset));
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

	    if (dirty)
	    {
	        // Remove old stuff
	        dirty = false;
	        ChangeToPreset();
	    }
	}

    private void SetPreset(Preset pPreset)
    {
        currentPreset = pPreset;
        currentPreset.selectedPreset = true;
        dirty = true;
    }



    private void ApplyPreset()
    {
        Debug.Log("Preset name: " + currentPreset.presetName + " Preset text: " + currentPreset.tickerText);
        TwitchOverlay.EventHandler.UpdateTickerText(currentPreset.tickerText);
        TwitchOverlay.EventHandler.ChangeBgColor(currentPreset.backgroundColor.GetUnityColor());
        TwitchOverlay.EventHandler.UpdateTickerTextColor(currentPreset.tickerTextColor.GetUnityColor());
        TwitchOverlay.EventHandler.UpdateTickerBackgroundColor(currentPreset.tickerBackgroundColor.GetUnityColor());
        TwitchOverlay.EventHandler.ReceivedSpeedAndDuration(currentPreset.tickerSpeed, currentPreset.tickerRepeatTime);
        TwitchOverlay.EventHandler.ReceivedTrayPreset(currentPreset.listOfTrays);
        TwitchOverlay.EventHandler.ReceivedGameImagePreset(currentPreset.gameImages);
    }

    private void ChangeToPreset()
    {
        //Check the one we changed from
        CheckPreviousPreset();
        // Check the one we changed to
        CheckPresetToSet();
        SaveObject.Instance.currentPreset.selectedPreset = false;
        SaveObject.Instance.currentPreset = currentPreset;
        ApplyPreset();
    }

    private void CheckPreviousPreset()
    { 
        if (SaveObject.Instance.currentPreset.presetName != "Reset preset")
        {
            bool found = false;
            for (int i = 0; i < SaveObject.Instance.listOfPresets.Count; i++)
            {
                if (SaveObject.Instance.currentPreset.presetName == SaveObject.Instance.listOfPresets[i].presetName)
                {
                    found = true;
                    SaveObject.Instance.listOfPresets[i] = SaveObject.Instance.currentPreset;
                    break;
                }
            }
            if (!found)
            {
                SaveObject.Instance.listOfPresets.Add(SaveObject.Instance.currentPreset);
            }
        }
    }

    private void CheckPresetToSet()
    {
        if (currentPreset.presetName != "Reset preset")
        {
            if (currentPreset.resetPreset)
            {
                for (int i = 0; i < SaveObject.Instance.listOfPresets.Count; i++)
                {
                    if (currentPreset.presetName == SaveObject.Instance.listOfPresets[i].presetName)
                    {
                        SaveObject.Instance.listOfPresets[i] = currentPreset;
                    }
                }
            }
            else
            {
                // update preset with new information
                for (int i = 0; i < SaveObject.Instance.listOfPresets.Count; i++)
                {
                    if (currentPreset.presetName == SaveObject.Instance.listOfPresets[i].presetName)
                    {
                        // update preset images and trays
                        UpdatePresetTrays(SaveObject.Instance.listOfPresets[i]);
                        UpdatePresetGameImages(SaveObject.Instance.listOfPresets[i]);
                        SaveObject.Instance.listOfPresets[i] = currentPreset;
                    }
                }
            }
        }
        SaveObject.Instance.currentPreset = currentPreset;
    }

    private void UpdatePresetTrays(Preset pPreset)
    {
        for (int i = 0; i < currentPreset.listOfTrays.Count; i++)
        {
            for (int j = 0; j < pPreset.listOfTrays.Count; j++)
            {
                if (currentPreset.listOfTrays[i].trayFilename == pPreset.listOfTrays[j].trayFilename)
                {
                    currentPreset.listOfTrays[i].trayTransform = pPreset.listOfTrays[j].trayTransform;
                }
            }
        }
    }

    private void UpdatePresetGameImages(Preset pPreset)
    {
        for (int i = 0; i < currentPreset.gameImages.Count; i++)
        {
            for (int j = 0; j < pPreset.gameImages.Count; j++)
            {
                if (currentPreset.gameImages[i].ObjectFileName == pPreset.gameImages[j].ObjectFileName)
                {
                    currentPreset.gameImages[i].position = pPreset.gameImages[j].position;
                    currentPreset.gameImages[i].rotation = pPreset.gameImages[j].rotation;
                    currentPreset.gameImages[i].scale = pPreset.gameImages[j].scale;
                }
            }
        }
    }




    /*
    Check current preset
        -> Preset in the preset list?
            -> Yes -> Update preset - Just overwrite.
            -> No  -> Add preset. Do nothing more with that.

First has the preset been sent before?
    Yes->Check if the user wants to reset it.
        Reset ? ->Yes->Overwrite the preset with the name.
                -> No->Find the existing preset.
                    ->Did we find the existing preset ?
                        ->Yes->update preset
                        ->No->Just add the preset.
No->Add the preset.

*/
    // First do we have the preset in the list?


}


//private void SavePreset()
//{
//    XmlSerializer formatter = new XmlSerializer(typeof(List<Preset>));
//    Stream stream = new FileStream(".\\" + presetFilename, FileMode.Create, FileAccess.Write,  FileShare.None);
//    formatter.Serialize(stream, loadedPresets);

//}


//if (!CheckSavedPreset())
//{
//    for (int i = 0; i < SaveObject.Instance.listOfPresets.Count; i++)
//    {
//        if (SaveObject.Instance.listOfPresets[i].presetName != pPreset.presetName)
//        {
//            SaveObject.Instance.listOfPresets[i].selectedPreset = false;
//        }
//    }


//    currentPreset = pPreset;
//    SaveObject.Instance.currentPreset = currentPreset;
//}

//dirty = true;
