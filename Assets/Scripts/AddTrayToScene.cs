﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class AddTrayToScene : MonoBehaviour
{

    public GameObject trayPrefab;
    private List<GameObject> listOfTrayObjects = new List<GameObject>();
    private bool dirty = false;
    private Tray lastAddedPackage = null;
    private Texture2D textureToAdd = null;
    private bool animationOnOffDirty = false;
    private int lastIdToTurnOnOff = -1;
    private bool autoAnimationDirty = false;
    private bool clearScene = false;
    private bool receivedPreset = false;
    private List<Tray> listOfPresetTrays;

    void Awake()
    {
        TwitchOverlay.EventHandler.OnAddTray += new TwitchOverlay.EventHandler.SingleArgDelegate((trayToAdd)=>AddTray((Tray)trayToAdd));
        TwitchOverlay.EventHandler.OnTurnOnOffTray += new TwitchOverlay.EventHandler.SingleArgDelegate((trayID)=>TurnTrayOnOff((int)trayID));
        TwitchOverlay.EventHandler.OnAutomateTray += new TwitchOverlay.EventHandler.SingleArgDelegate((trayID)=>SetTrayAutomation((int)trayID));
        TwitchOverlay.EventHandler.OnSetPresetTray += new TwitchOverlay.EventHandler.SingleArgDelegate((listoftrayPresets)=>SetPresetTray((List<Tray>)listoftrayPresets));
    }




    // Use this for initialization
    void Start () {
	    //TwitchOverlay.EventHandler.OnAddTray += new 
	}
	
	// Update is called once per frame
	void Update () {
	    if (clearScene)
	    {
	        ClearScene();
	        AddTraysToScene();
	    }

        if (dirty)
	    {
            AddTrayImage();
	    }
	    if (animationOnOffDirty)
	    {
            ManualAnimationOnOff();
        }

	    if (autoAnimationDirty)
	    {
            AutoAnimate();
        }
    }


    private void AutoAnimate()
    {
        autoAnimationDirty = false;
        GameObject obj = null;
        for (int i = 0; i < listOfTrayObjects.Count; i++)
        {
            if (listOfTrayObjects[i].GetComponent<ModifyTray>().GetTray().trayID == lastIdToTurnOnOff)
            {
                obj = listOfTrayObjects[i];
                break;
            }
        }
        if (obj != null)
        {
            Tray package = obj.GetComponent<ModifyTray>().GetTray();
            obj.GetComponent<TrayAnimationHandler>().SetAutomationTime(package.trayFadeInIntervalFromSeconds, package.trayFadeInIntervalToSeconds, package.trayFadeOutIntervalFromSeconds, package.trayFadeOutIntervalToSeconds);
            obj.GetComponent<TrayAnimationHandler>().SetAutoAnimate();
        }
    }

    private void ManualAnimationOnOff()
    {
        animationOnOffDirty = false;
        GameObject obj = null;
        for (int i = 0; i < listOfTrayObjects.Count; i++)
        {
            if (listOfTrayObjects[i].GetComponent<ModifyTray>().GetTray().trayID == lastIdToTurnOnOff)
            {
                obj = listOfTrayObjects[i];
                break;
            }
        }
        if (obj != null)
        {
            obj.GetComponent<TrayAnimationHandler>().TurnOnOffTray();
        }
    }

    private void AddTraysToScene()
    {
        for (int i = 0; i < listOfPresetTrays.Count; i++)
        {
            byte[] buffer = File.ReadAllBytes(listOfPresetTrays[i].trayFilename);
            if (buffer.Length > 0)
            {
                textureToAdd = new Texture2D(512, 512, TextureFormat.DXT5, false);
                textureToAdd.LoadImage(buffer);

                GameObject addedTrayObj = GameObject.Instantiate(trayPrefab, listOfPresetTrays[i].trayTransform.GetPositionAsVector3(), listOfPresetTrays[i].trayTransform.GetRotationAsQuaternion()) as GameObject;
                SpriteRenderer goRenderer = addedTrayObj.GetComponent<SpriteRenderer>();
                goRenderer.sprite = Sprite.Create(textureToAdd, new Rect(0, 0, textureToAdd.width, textureToAdd.height), new Vector2(0.5f, 0.5f));
                BoxCollider2D collider = addedTrayObj.GetComponent<BoxCollider2D>();
                collider.size = new Vector2(textureToAdd.width / 100.0f, textureToAdd.height / 100.0f);
                Debug.Log("width " + textureToAdd.width + " height: " + textureToAdd.height);
                addedTrayObj.GetComponent<ModifyTray>().SetTray(listOfPresetTrays[i]);
                addedTrayObj.transform.position = listOfPresetTrays[i].trayTransform.GetPositionAsVector3();
                addedTrayObj.transform.rotation = listOfPresetTrays[i].trayTransform.GetRotationAsQuaternion();
                addedTrayObj.transform.localScale = listOfPresetTrays[i].trayTransform.GetScaleAsVector3();
                listOfTrayObjects.Add(addedTrayObj);
                if (listOfPresetTrays[i].trayUseAutoAnimate)
                {
                    addedTrayObj.GetComponent<TrayAnimationHandler>().SetAutomationTime(listOfPresetTrays[i].trayFadeInIntervalFromSeconds, listOfPresetTrays[i].trayFadeInIntervalToSeconds, listOfPresetTrays[i].trayFadeOutIntervalFromSeconds, listOfPresetTrays[i].trayFadeOutIntervalToSeconds);
                    addedTrayObj.GetComponent<TrayAnimationHandler>().SetAutoAnimate();
                }
            }
        }
    }


    private void AddTrayImage()
    {
        bool found = false;
        GameObject goToChange;
        for (int i = 0; i < listOfTrayObjects.Count; i++)
        {
            if (listOfTrayObjects[i].GetComponent<ModifyTray>().GetTray().trayID == lastAddedPackage.trayID)
            {
                found = true;
                goToChange = listOfTrayObjects[i];
                goToChange.GetComponent<ModifyTray>().SetTray(lastAddedPackage);
                break;
            }
        }
        dirty = false;
        if (!found)
        {
            byte[] buffer = File.ReadAllBytes(lastAddedPackage.trayFilename);
            if (buffer.Length > 0)
            {
                textureToAdd = new Texture2D(512, 512, TextureFormat.DXT5, false);
                textureToAdd.LoadImage(buffer);

                GameObject addedTrayObj = GameObject.Instantiate(trayPrefab);
                SpriteRenderer goRenderer = addedTrayObj.GetComponent<SpriteRenderer>();
                goRenderer.sprite = Sprite.Create(textureToAdd, new Rect(0, 0, textureToAdd.width, textureToAdd.height), new Vector2(0.5f, 0.5f));
                BoxCollider2D collider = addedTrayObj.GetComponent<BoxCollider2D>();
                collider.size = new Vector2(textureToAdd.width / 100.0f, textureToAdd.height / 100.0f);
                addedTrayObj.GetComponent<ModifyTray>().SetTray(lastAddedPackage);
                listOfTrayObjects.Add(addedTrayObj);
                if (lastAddedPackage.trayUseAutoAnimate)
                {
                    SetTrayAutomation(lastAddedPackage.trayID);
                }
            }
        }
    }


    private void AddTray(Tray pTray)
    {
        dirty = true;
        lastAddedPackage = pTray;
    }

    private void TurnTrayOnOff(int pID)
    {
        animationOnOffDirty = true;
        lastIdToTurnOnOff = pID;
    }

    private void SetTrayAutomation(int trayID)
    {
        autoAnimationDirty = true;
        lastIdToTurnOnOff = trayID;
    }

    private void SetPresetTray(List<Tray> pPreseTraytList)
    {
        clearScene = true;
        listOfPresetTrays = pPreseTraytList;
    }

    private void ClearScene()
    {
        lastIdToTurnOnOff = -1;
        clearScene = false;
        for (int i = 0; i < listOfTrayObjects.Count; i++)
        {
            GameObject.Destroy(listOfTrayObjects[i]);
        }
        listOfTrayObjects.Clear();
        lastAddedPackage = null;
    }

}
