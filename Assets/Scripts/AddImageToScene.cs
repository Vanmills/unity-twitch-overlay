﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.EventSystems;
using EventHandler = TwitchOverlay.EventHandler;

public class AddImageToScene : MonoBehaviour
{
    public GameObject imagePrefab;
    private string filename = "";
    private bool dirty = false;
    private int id = 0;

    private bool presetReceived = true;
    private List<GameObject> gameObjectsInScene = new List<GameObject>();
    private List<MyGameObject>  presetImages = new List<MyGameObject>();

    // Use this for initialization
    void Start () {
	    TwitchOverlay.EventHandler.OnAddImage += new TwitchOverlay.EventHandler.SingleArgDelegate((filenameAndPath)=>AddImage((string)filenameAndPath));
        TwitchOverlay.EventHandler.OnSetPresetGameImages += new TwitchOverlay.EventHandler.SingleArgDelegate((listOfImages)=>AddImagesFromPreset((List<MyGameObject>)listOfImages));
	}
	
	// Update is called once per frame
	void Update () {

	    if (dirty)
	    {
	        dirty = false;
	        byte[] buffer = File.ReadAllBytes(filename);
	        if (buffer.Length > 0)
	        {
                Texture2D texture = new Texture2D(512, 512, TextureFormat.DXT5, false);
	            texture.LoadImage(buffer);
                GameObject go = GameObject.Instantiate(imagePrefab);
	            SpriteRenderer goRenderer = go.GetComponent<SpriteRenderer>();
                goRenderer.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
	            BoxCollider2D collider = go.GetComponent<BoxCollider2D>();
                collider.size = new Vector2(texture.width / 100.0f, texture.height / 100.0f);
                MyGameObject obj = new MyGameObject();
	            obj.ObjectFileName = filename;
                obj.SetParameters(go.transform.position, go.transform.rotation, go.transform.localScale);
	            obj.ID = id;
                go.GetComponent<ModifyImage>().SetMyGameObject(obj);
                SaveObject.Instance.currentPreset.gameImages.Add(obj);
                gameObjectsInScene.Add(go);
	            id++;
	        }
	        else
	        {
                Debug.Log("Filename: " + filename);
	            return;
	        }

	    }

	    if (presetReceived)
	    {
            presetReceived = false;
	        ClearScene();
	        if (presetImages.Count > 0)
	        {
	            LoadImages();
	        }
	    }

	}

    private void ClearScene()
    {
        for (int i = 0; i < gameObjectsInScene.Count; i++)
        {
            GameObject.Destroy(gameObjectsInScene[i]);
        }
        gameObjectsInScene.Clear();

    }

    private void LoadImages()
    {
        
        for (int i = 0; i < presetImages.Count; i++)
        {
            byte[] buffer = File.ReadAllBytes(presetImages[i].ObjectFileName);
            if (buffer.Length > 0)
            {
                Texture2D texture = new Texture2D(512, 512, TextureFormat.DXT5, false);
                texture.LoadImage(buffer);
                Vector3 position = presetImages[i].GetPositionAsVector3();
                Vector3 scale = presetImages[i].GetScaleAsVector3();
                Quaternion rotation = presetImages[i].GetRotationAsQuaternion();
                GameObject go = GameObject.Instantiate(imagePrefab, position, rotation) as GameObject;
                go.transform.localScale = scale;
                SpriteRenderer goRenderer = go.GetComponent<SpriteRenderer>();
                goRenderer.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                BoxCollider2D collider = go.GetComponent<BoxCollider2D>();
                collider.size = new Vector2(texture.width / 100.0f, texture.height / 100.0f);
                presetImages[i].ID = id;
                go.GetComponent<ModifyImage>().SetMyGameObject(presetImages[i]);
                //SaveObject.Instance.currentPreset.gameImages.Add((listOfObjectsToLoad[i]));
                id = presetImages[i].ID + 1;
                gameObjectsInScene.Add(go);
            }
            Debug.Log("Loaded image: " + presetImages[i].ObjectFileName);
        }

    }

    private void AddImage(string pFilename)
    {
        dirty = true;
        filename = pFilename;
    }

    private void AddImagesFromPreset(List<MyGameObject> pImageList)
    {
        presetImages = pImageList;
        presetReceived = true;
        id = 0;
    }

}
