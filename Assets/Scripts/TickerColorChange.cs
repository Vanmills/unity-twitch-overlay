﻿using UnityEngine;
using System.Collections;

public class TickerColorChange : MonoBehaviour
{

    private bool dirty = false;
    public Sprite refSprite;
    private Sprite sprite;
    private Color color;
    private Material material;

    void Awake()
    {
        material = GetComponent<SpriteRenderer>().material;
        TwitchOverlay.EventHandler.OnUpdateTickerBGColor += new TwitchOverlay.EventHandler.SingleArgDelegate((bgColor)=> UpdateTickerColor((Color)bgColor)); 
    }

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update () {

	    if (dirty)
	    {
	        dirty = false;
            Texture2D texture2d = new Texture2D(refSprite.texture.width, refSprite.texture.height);
            Debug.Log("w: " + texture2d.width + " h: " + texture2d.height);
            Color[] colors = new Color[texture2d.width * texture2d.height];
            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = color;
            }
            texture2d.SetPixels(0, 0, refSprite.texture.width, refSprite.texture.height, colors);
            //texture2d.alphaIsTransparency = false;
            texture2d.Apply();

            sprite = Sprite.Create(texture2d, refSprite.textureRect, new Vector2(0.5f, 0.5f), 92.0f, 0);
            sprite.name = "Mysprite";
	        
            GetComponent<SpriteRenderer>().sprite = sprite;
            
	        GetComponent<SpriteRenderer>().material = material;
            //gameObject.AddComponent<TickerFadeOut>();
	        //GetComponent<SpriteRenderer>().enabled = SaveObject.Instance.TickerBarOn;
        }
    }


    private void UpdateTickerColor(Color pColor)
    {
        color = pColor;
        dirty = true;
        SaveObject.Instance.currentPreset.tickerBackgroundColor = new UserColor(pColor.r, pColor.g, pColor.b, pColor.a);
    }

}
