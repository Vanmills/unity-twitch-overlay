﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    //[ExecuteInEditMode]
    public class TextureMeshDrawPriority : MonoBehaviour
    {
        public int orderInLayer = 0;
        public SortingLayer layer;
	    // Use this for initialization
	    void Start ()
	    {
	        gameObject.GetComponent<MeshRenderer>().sortingLayerName = "Tray";
            gameObject.GetComponent<MeshRenderer>().sortingOrder = orderInLayer;

	    }

    }
}
