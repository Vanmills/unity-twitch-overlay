﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Linq;

public class ModifyImage : MonoBehaviour
{
    public GameObject selectionGO;


    private bool selected = false;
    private MyGameObject addedImage;
    private Texture2D selectionTexture = null;
    //public bool isGameLogo = false;
    private Vector3 offset = Vector3.zero;
    // Use this for initialization
    void Start () {


	}
	
	// Update is called once per frame
	void Update () {
	    if (selected)
	    {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition) + offset;

            pos.z = gameObject.transform.position.z;
            gameObject.transform.position = pos;
	        if (Input.GetKey(KeyCode.R))
	        {
	            transform.localScale += new Vector3(0.5f * Time.deltaTime, 0.5f * Time.deltaTime, 0.5f * Time.deltaTime);
	        }  else if (Input.GetKey(KeyCode.Q))
	        {
	            if (transform.localScale.x > 0.1f && transform.localScale.y > 0.1f)
	            {
                    transform.localScale -= new Vector3(0.5f * Time.deltaTime, 0.5f * Time.deltaTime, 0.5f * Time.deltaTime);
	            }
            }

	        if (Input.GetKeyDown(KeyCode.Delete))
	        {
	            for (int i = 0; i < SaveObject.Instance.currentPreset.gameImages.Count; i++)
	            {
	                if (SaveObject.Instance.currentPreset.gameImages[i].ID == addedImage.ID)
	                {
	                    SaveObject.Instance.currentPreset.gameImages.Remove(SaveObject.Instance.currentPreset.gameImages[i]);
	                }
	            }
	            GameObject.Destroy(gameObject);
	        }

	        if (Input.GetKey(KeyCode.C))
	        {
	            Vector3 rotate = transform.rotation.eulerAngles;
	            rotate.z += 45.0f * Time.deltaTime;
	            transform.rotation = Quaternion.Euler(rotate);
	        } else if (Input.GetKey(KeyCode.Z))
	        {
                Vector3 rotate = transform.rotation.eulerAngles;
                rotate.z -= 45.0f * Time.deltaTime;
                transform.rotation = Quaternion.Euler(rotate);
            } else if (Input.GetKeyDown(KeyCode.X))
            {
                Vector3 rotate = transform.rotation.eulerAngles;
                rotate.z = (int) rotate.z;
                int clamper = (int)rotate.z/15;
                rotate.z = clamper*15;
                transform.rotation = Quaternion.Euler(rotate);

            }
	        if (Input.GetKeyDown(KeyCode.Minus) || Input.GetKeyDown(KeyCode.KeypadMinus))
	        {
	            GetComponent<SpriteRenderer>().sortingOrder -= 1;
	        } else if (Input.GetKeyDown(KeyCode.Plus) || Input.GetKeyDown(KeyCode.KeypadPlus))
	        {
	            GetComponent<SpriteRenderer>().sortingOrder += 1;
	        }
	    }

	    if (Input.GetKey(KeyCode.Escape) && Input.GetKey(KeyCode.LeftShift))
	    {
	        Application.Quit();
	    }
       

	}

    private void OnMouseDown()
    {
        selected = true;
        selectionGO.SetActive(true);
        
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z));
        if (selectionTexture == null)
        {
            Texture2D imageTexture = GetComponent<SpriteRenderer>().sprite.texture;
            selectionTexture = new Texture2D(imageTexture.width, imageTexture.height);
            Debug.Log("Selection texture w: " + selectionTexture.width + " h: " + selectionTexture.height);
            Color[] colors = new Color[selectionTexture.width * selectionTexture.height];

            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = Color.red;
                colors[i].a = 0.25f;
            }

            selectionTexture.SetPixels(colors);
            selectionTexture.Apply();
            Debug.Log("Selection texture color count: " + colors.Length);

            selectionGO.transform.parent = gameObject.transform;
            Sprite sprite = Sprite.Create(selectionTexture, GetComponent<SpriteRenderer>().sprite.textureRect, new Vector2(0.5f, 0.5f));
            sprite.name = "SelectionBox" + addedImage.ID + Path.GetFileNameWithoutExtension(addedImage.ObjectFileName);
            selectionGO.GetComponent<SpriteRenderer>().sprite = sprite;
            selectionGO.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1002;
            selectionGO.transform.position = transform.position;
            selectionGO.transform.rotation = transform.rotation;
            //selectionGO.transform.localScale = transform.localScale;
            Debug.Log("Selection box created");
        }
        else if (selectionTexture != null)
        {
            selectionGO.SetActive(true);
        }
    }

    void OnMouseUp()
    {
        selectionGO.SetActive(false);
        if (selected)
        {
            int i = 0;
            for (; i < SaveObject.Instance.currentPreset.gameImages.Count; i++)
            {
                if (SaveObject.Instance.currentPreset.gameImages[i].ID == addedImage.ID)
                {
                    break;
                }
            }
            if (selectionGO != null)
            {
                selectionGO.SetActive(false);
            }
            SaveObject.Instance.currentPreset.gameImages[i].SetParameters(transform.position, transform.rotation, transform.localScale);
            SaveObject.Instance.currentPreset.gameImages[i].sortingOrder = GetComponent<SpriteRenderer>().sortingOrder;
        }
        selected = false;

        //if (gameObject.name == "GameImage")
        //{
        //    isGameLogo = true;
        //}
        //if (isGameLogo)
        //{
        //    SaveObject.Instance.GameImagePosition = new MyVector3(transform.position);
        //    SaveObject.Instance.GameImageScale = new MyVector3(transform.localScale);
        //}
    }

    public void SetMyGameObject(MyGameObject obj)
    {
        addedImage = obj;
    }

}




//for (int y = 0; y < selectionTexture.height; y++)
//{
//    for (int x = 0; x < selectionTexture.width; x++)
//    {
//        if (x < selectionTexture.width && y == 0)
//        {
//            colors[x] = Color.red;
//        }
//        else if (y > 0 && x == 0)
//        {
//            colors[selectionTexture.width * y] = Color.red;
//        }
//        else if (selectionTexture.height - 2 == y)
//        {
//            colors[y * selectionTexture.width + x] = Color.red;
//        }
//        else if (x == (selectionTexture.width -1)&& y > 0)
//        {
//            colors[y * selectionTexture.width + (selectionTexture.width -2)] = Color.red;
//        }
//    }
//}


