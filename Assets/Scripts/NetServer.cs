﻿using System;
using UnityEngine;
using System.Collections;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;

public class NetServer
{
    //private Socket socket;
    private UdpClient server;
    private TcpListener serverListener;
    private IPEndPoint serverEndPoint;
    private IPEndPoint clientEndpoint;
    private Thread readThread;
    private bool isRunning = true;
    //public NetServer()
    //{ } // We don't want to use the default constructor only

    public void Open(string ipString, int port)
    {
        serverEndPoint = new IPEndPoint(IPAddress.Parse(ipString), port);
        server = new UdpClient(serverEndPoint);

        readThread = new Thread(ReadData);
        readThread.Start();
    }

    public void ReadData()
    {
        while (isRunning)
        {
            if (server.Available > 0)
            {
                byte[] buffer; // = new byte[socket.Available];
                PipePackage packet = new PipePackage();
                buffer = server.Receive(ref clientEndpoint);
                packet.DecodeBuffer(buffer);
                Debug.Log("Data received from: " + clientEndpoint.ToString() + " And sent to the event");
                TwitchOverlay.EventHandler.IncomingPackage(packet);

            }
            Thread.Sleep(1000/30);
        }
 
    }

    public void Close()
    {
        if (server != null)
        {
            server.Close();
        }
    }






}


// graveyard
//socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
//if (!socket.Connected || !socket.IsBound)
//{
//    try
//    {
//        //socket.Bind(serverEndPoint);
//        socket.Bind(serverEndPoint);
//    }
//    catch (SocketException e)
//    {
//        Debug.Log("Error: " + e.Message);
//        if (socket.IsBound)
//        {
//            Debug.Log("Socket already bound");
//        }
//    }
//}
//clientEndpoint = new IPEndPoint(IPAddress.Any, 0);
//Debug.Log(serverEndPoint.ToString());

//Debug.Log("Received data from client: " + clientEndpoint.ToString());
//Debug.Log("PipePackage created");
//Debug.Log("Packet decoded");
//Debug.Log("After client endpoint");
//Debug.Log("Data received");
//Debug.Log("Data available");
//Debug.Log("Data from socket received");
//Debug.Log("Before client endpoint");
//Debug.Log("Event ran successfully");
//if (socket.IsBound)
//{
//    if (socket.Connected)
//    {
//        socket.Shutdown(SocketShutdown.Both);
//    }
//    socket.Close();

//}