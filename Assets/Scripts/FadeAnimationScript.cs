﻿using UnityEngine;
using System.Collections;
using System.Text;

public class FadeAnimationScript : MonoBehaviour
{

    private bool fadeIn = false;
    private float fadeInSteps = 0.0f;
    private float fadeInCounter = 2.0f;

    private bool fadeOut = false;
    private float fadeOutSteps = 0.0f;
    private float fadeOutCounter = 2.0f;

	// Update is called once per frame
	void Update () {
	    if (fadeIn)
	    {
	        fadeInCounter -= Time.deltaTime;
            if (fadeInCounter <= 0.0f)
            {
                fadeIn = false;
            }
            else
            {
                Color color = GetComponent<SpriteRenderer>().material.color;
                color.a += fadeInSteps * Time.deltaTime;
                if (color.a >= 1.0f)
                {
                    fadeIn = false;
                    color.a = 1.0f;
                }
                GetComponent<SpriteRenderer>().material.color = color;
            }
	    }

	    if (fadeOut)
	    {
	        fadeOutCounter -= Time.deltaTime;
	        if (fadeOutCounter <= 0.0f)
	        {
	            fadeOut = false;
	        }
	        else
	        {
	            Color color = GetComponent<SpriteRenderer>().material.color;
	            color.a -= color.a*fadeOutSteps*Time.deltaTime;
	            if (color.a <= 0.0f)
	            {
	                fadeOut = false;
	                color.a = 0.0f;
                    
	            }
	            GetComponent<SpriteRenderer>().material.color = color;
	        }
	    }

	}

    public void StartFadeIn(float pDuration, bool pFadeIn = true)
    {
        fadeIn = pFadeIn;
        fadeInCounter = pDuration;
        if (pDuration > 0.0f)
        {
            fadeInSteps = 1.0f/fadeInCounter;
        }
        else
        {
            fadeIn = false;
        }
        Color color = GetComponent<SpriteRenderer>().material.color;
        if (fadeIn)
        {
            color.a = 0.0f;
        }
        else
        {
            color.a = 1.0f;
        }
        GetComponent<SpriteRenderer>().material.color = color;

    }

    public void StartFadeOut(float pDuration, bool pFadeOut)
    {
        fadeOut = pFadeOut;
        fadeOutCounter = pDuration;
        if (pDuration >= 0.0f)
        {
            fadeOutSteps = 1.0f/fadeOutCounter;
        }
        else
        {
            fadeOut = false;
        }
    }


}
