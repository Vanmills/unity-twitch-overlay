﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class TicketAnimatorController : MonoBehaviour
{

    public TextMesh text1 = null;

    public bool dirty = false;
    public bool onOffDirty = false;
    private string tickerText;

    private float tickerDisplayCountdown;
    private float tickerDisplayTime = 45.0f;
    private float animSpeed = 0.1f;

    void Awake()
    {
        TwitchOverlay.EventHandler.OnUpdateTickerText += new TwitchOverlay.EventHandler.SingleArgDelegate((receiveTickerText) => GetText((string)receiveTickerText));
        TwitchOverlay.EventHandler.OnTurnOnOffTicker += new TwitchOverlay.EventHandler.NoArgDelegate(TurnOnOffTicker);
        TwitchOverlay.EventHandler.OnReceiveSpeedAndDuration += new TwitchOverlay.EventHandler.DualArgDelegate((animSpeed, animTime) => SetSpeedAndRepeatTime((float)animSpeed, (float)animTime));
    }

	// Use this for initialization
	void Start ()
	{
	    tickerDisplayCountdown = tickerDisplayTime;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (text1.gameObject.activeSelf && !string.IsNullOrEmpty(tickerText))
	    {
	        tickerDisplayCountdown -= Time.deltaTime;
	        if (tickerDisplayCountdown < 0.0f)
	        {
	            dirty = true;
	            tickerDisplayCountdown = tickerDisplayTime;
	        }
        }

	    if (dirty)
	    {
	        dirty = false;
            if (text1 != null)
	        {
	            text1.GetComponent<TickerAnimation>().SetText(tickerText);
                text1.GetComponent<TickerAnimation>().SetSpeed(animSpeed);
	            if (text1.gameObject.activeSelf == false)
	            {
	                onOffDirty = true;
	            }
	        }
	    }
        if (onOffDirty)
        {
            onOffDirty = false;
            if (text1 != null)
            {
                text1.gameObject.SetActive(!text1.gameObject.activeSelf);
            }
            SaveObject.Instance.currentPreset.useTickerText = text1.gameObject.activeSelf;
        }

    }

    void GetText(string pText)
    {
        tickerText = pText;
        dirty = true;
        SaveObject.Instance.currentPreset.tickerText = tickerText;
    }

    private void TurnOnOffTicker()
    {
        onOffDirty = true;
    }

    private void SetSpeedAndRepeatTime(float pSpeed, float pTime)
    {
        if (pSpeed <= 0)
        {
            pSpeed = 0.1f;
        }
        tickerDisplayTime = pTime;
        animSpeed = pSpeed / 1000.0f;
        tickerDisplayCountdown = 0.0f;
        SaveObject.Instance.currentPreset.tickerRepeatTime = pTime;
        SaveObject.Instance.currentPreset.tickerSpeed = pSpeed;
    }

}
