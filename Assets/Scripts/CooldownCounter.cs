﻿using System;
using UnityEngine;
using System.Collections;

public class CooldownCounter : MonoBehaviour
{

    private TextMesh textMesh;
    public float cooldown = 5955.0f;
	// Use this for initialization
	void Start ()
	{
	    textMesh = GetComponent<TextMesh>();

	}
	
	// Update is called once per frame
	void Update () {
        cooldown -= Time.deltaTime;
        if (cooldown <= 0.0f)
	    {
	        textMesh.text = "Timer Expired!";
	    }
	    else
	    {
	        string displayTime = "";
	        int hours = (int)(cooldown / 3600.0f);
            int minutes =(int)((cooldown - hours * 3600.0f) / 60.0f);
            int seconds = (int)(cooldown - (hours * 60 * 60) - (minutes * 60));
            displayTime = hours.ToString().PadLeft(2, '0') + ":" + minutes.ToString().PadLeft(2, '0') + ":" + seconds.ToString().PadLeft(2, '0');
	        textMesh.text = displayTime;
	    }
	}


    public void SetCooldown(int pCooldown)
    {
        cooldown = pCooldown*60.0f;

    }


}
