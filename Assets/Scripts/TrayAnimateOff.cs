﻿using UnityEngine;
using System.Collections;
using System.Runtime.Remoting.Messaging;

public class TrayAnimateOff : MonoBehaviour {

    private Vector3 trayStartPosition;
    private Vector3 trayEndPosition;
    public bool startAnimation = false;

    public float speed = 1.5f;
    private TrayDirection animDirection;

    void Awake()
    {
        trayEndPosition.x = 13.0f;
        trayEndPosition.y = trayStartPosition.y;
        trayEndPosition.z = trayStartPosition.z;
        trayStartPosition = transform.position;
        animDirection = TrayDirection.Right;
    }

    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        if (startAnimation)
        {
            Vector3 pos = transform.position;
            if (animDirection == TrayDirection.Up)
            {
                float yPos = Mathf.Lerp(transform.position.y, trayEndPosition.y, Time.deltaTime*speed);
                if (trayEndPosition.y - yPos <= 0.01f)
                {
                    startAnimation = false;
                    return;
                }
                pos.y = yPos;

            } else if (animDirection == TrayDirection.Down)
            {
                float yPos = Mathf.Lerp(transform.position.y, trayEndPosition.y, Time.deltaTime * speed);
                if (yPos - trayEndPosition.y <= 0.01f)
                {
                    startAnimation = false;
                    return;
                }
                pos.y = yPos;


            } else if (animDirection == TrayDirection.Left)
            {
                float xPos = Mathf.Lerp(transform.position.x, trayEndPosition.x, Time.deltaTime*speed);
                if (xPos - trayEndPosition.x <= 0.01f)
                {
                    startAnimation = false;
                    return;
                }
                pos.x = xPos;

            } else if (animDirection == TrayDirection.Right)
            {
                float xPos = Mathf.Lerp(pos.x, trayEndPosition.x, speed * Time.deltaTime);
                if (trayEndPosition.x - xPos <= 0.01f)
                {
                    startAnimation = false;
                    return;
                }
                pos.x = xPos;
            }
            transform.position = pos;



        }
    }

    public void StartAnimation()
    {
        startAnimation = true;
        GetComponent<TrayAnimateOn>().StopAnimation();
    }

    public void SetStartPosition(Vector3 pStartPos)
    {
        trayStartPosition = pStartPos;
    }

    public void SetAnimationDirection(TrayDirection pDirection)
    {
        animDirection = pDirection;
        if (animDirection == TrayDirection.Right)
        {
            trayEndPosition = new Vector3(23.0f, 0.0f, 0.0f);
        }
        else if (animDirection == TrayDirection.Left)
        {
            trayEndPosition = new Vector3(-23.0f, 0.0f, 0.0f);
        }
        else if (animDirection == TrayDirection.Up)
        {
            trayEndPosition = new Vector3(0.0f, 18.0f, 0.0f);
        }
        else if (animDirection == TrayDirection.Down)
        {
            trayEndPosition = new Vector3(0.0f, -18.0f, 0.0f);
        }
    }

    public void SetAnimationSpeed(int pSpeed)
    {
        speed = (float) pSpeed/10.0f;
    }

    public void StopAnimation()
    {
        startAnimation = false;
    }
}
