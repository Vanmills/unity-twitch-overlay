﻿using UnityEngine;
using System.Collections;

public class SpinZoomAnimationController : MonoBehaviour
{

    private ImageAnimation imageAnimation;
    private float animationDuration = 0.0f;
    private float animationTimer = 0.0f;
    private float zoomInTimer;
    private float zoomOutTimer;
    private float fadeInTimer;
    private float fadeOutTimer;
    private float rotateInTimer;
    private float rotateOutTimer;

	// Update is called once per frame
	void Update ()
	{
	    animationTimer  += Time.deltaTime;
        zoomInTimer     -= Time.deltaTime;
        zoomOutTimer    -= Time.deltaTime;
        fadeInTimer     -= Time.deltaTime;
        fadeOutTimer    -= Time.deltaTime;
        rotateInTimer   -= Time.deltaTime;
        rotateOutTimer  -= Time.deltaTime;

	    if (zoomInTimer <= 0.0f)
	    {
	        zoomInTimer = animationDuration * 2.0f;
            GetComponent<ZoomAnimationScript>().StartZoomIn(imageAnimation.zoomInSize, imageAnimation.zoomInSpeed, imageAnimation.zoomIn);
	    }
	    if (zoomOutTimer <= 0.0f)
	    {
            GetComponent<ZoomAnimationScript>().StartZoomOut(imageAnimation.zoomOutSize, imageAnimation.zoomOutSpeed, imageAnimation.zoomOut);
	        zoomOutTimer = animationDuration * 2.0f;
	    }

	    if (fadeInTimer <= 0.0f)
	    {
	        fadeInTimer = animationDuration * 2.0f;
            GetComponent<FadeAnimationScript>().StartFadeIn(imageAnimation.fadeInDuration, imageAnimation.fadeIn);
	    }
	    if (fadeOutTimer <= 0.0f)
	    {
	        fadeOutTimer = animationDuration * 2.0f;
            GetComponent<FadeAnimationScript>().StartFadeOut(imageAnimation.fadeOutDuration, imageAnimation.fadeOut);
	    }

	    if (rotateInTimer <= 0.0f)
	    {
	        rotateInTimer = animationDuration * 2.0f;
            GetComponent<RotationAnimationScript>().StartRotateIn(imageAnimation.rotationInDegrees, imageAnimation.rotateInDuration, imageAnimation.rotateInClockwise, imageAnimation.rotateIn);
	    }
	    if (rotateOutTimer <= 0.0f)
	    {
	        rotateOutTimer = animationDuration * 2.0f;
            GetComponent<RotationAnimationScript>().StartRotateOut(imageAnimation.rotationOutDegrees, imageAnimation.rotateOutDuration, imageAnimation.rotateOutClockwise, imageAnimation.rotateOut);
        }


        if (animationTimer >= animationDuration)
	    {
            GameObject.Destroy(gameObject, 0.01f);
        }
	}

    public void SetAnimationParameters(ImageAnimation pParameters)
    {
        imageAnimation = pParameters;
        animationDuration = imageAnimation.totalDuration;
        zoomInTimer     =  imageAnimation.zoomInStartTime;
        zoomOutTimer    =  imageAnimation.zoomOutStartTime;
        fadeInTimer     =  imageAnimation.fadeInStartTime;
        fadeOutTimer    =  imageAnimation.fadeOutStartTime;
        rotateInTimer   =  imageAnimation.rotateInStartTime;
        rotateOutTimer  =  imageAnimation.rotateOutStartTime;
        animationTimer = 0.0f;

    }


}
