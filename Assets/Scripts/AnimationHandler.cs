﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

public class AnimationHandler : MonoBehaviour
{

    public ImageAnimation spinZoomPackage = new ImageAnimation();
    public GameObject AnimationObjectPrefab;
    private Texture2D texture;

    public bool dirty = false;

    //private List<GameObject> listOfSpawnedObjects = new List<GameObject>();

    void Awake()
    {
        TwitchOverlay.EventHandler.OnAnimateImage += new TwitchOverlay.EventHandler.SingleArgDelegate((animatedImage)=> SetSpinZoomPackage((ImageAnimation)animatedImage));
    }

    void Update()
    {
        if (dirty)
        {
            dirty = false;
            GameObject obj = GameObject.Instantiate(AnimationObjectPrefab, Vector3.zero,
                Quaternion.Euler(0.0f, 0.0f, 0.0f)) as GameObject;
            obj.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
            if (!string.IsNullOrEmpty(spinZoomPackage.filenameGraphic))
            {
                SpriteRenderer spriteRenderer = obj.GetComponent<SpriteRenderer>();
                Sprite sprite = spriteRenderer.sprite;

                byte[] imageData = File.ReadAllBytes(spinZoomPackage.filenameGraphic);
                texture = new Texture2D(sprite.texture.width, sprite.texture.height);
                texture.LoadImage(imageData);
                texture.Apply();
                sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                sprite.name = "CustomImage";
                spriteRenderer.sprite = sprite;
            }
            obj.GetComponent<SpinZoomAnimationController>().SetAnimationParameters(spinZoomPackage);

        }

    }


    public void StartAnimation()
    {
        dirty = true;
    }

    public void SetSpinZoomPackage(ImageAnimation pSpzPackage)
    {
        spinZoomPackage = pSpzPackage;
        dirty = true;
        
    }

    public ImageAnimation GetCurrentSpinZoomPackage()
    {
        return spinZoomPackage;
    }


}



/*
        totalAnimationDurationCounter = spinZoomPackage.totalDuration;

        startFadeOutCounter = spinZoomPackage.totalDuration - spinZoomPackage.startFadeInTime - spinZoomPackage.fadeOutTime;
        startRotationCounter = spinZoomPackage.totalDuration - spinZoomPackage.startRotationInTime - spinZoomPackage.rotateOutTime;
        startZoomCounter = spinZoomPackage.totalDuration -spinZoomPackage.startZoomInAnimation - spinZoomPackage.zoomOutTime;

        waitTimeForFadeOut = spinZoomPackage.fadeOutTime;
        waitTimeForRotation = spinZoomPackage.startRotationInTime;
        waitTimeForZoom = spinZoomPackage.startZoomInAnimation;
        rotationInSteps = spinZoomPackage.totalDegreesToRotateIn /(spinZoomPackage.totalDuration - spinZoomPackage.startRotationInTime - spinZoomPackage.rotateOutTime);
        rotationOutSteps = spinZoomPackage.totalDegreesToRotateOut/(spinZoomPackage.rotateOutTime);
        if (spinZoomPackage.zoomToTarget == 0.0f)
        {
            spinZoomPackage.zoomToTarget = 1.0f;
        }
        zoomSteps = (spinZoomPackage.totalDuration - spinZoomPackage.startZoomInAnimation) / spinZoomPackage.zoomToTarget;
        if (spinZoomPackage.fadeOutTime != 0.0f)
        {
            fadeOutSteps = 1.0f/(spinZoomPackage.totalDuration - spinZoomPackage.fadeOutTime - spinZoomPackage.startFadeInTime);
        }
        else
        {
            fadeOutSteps = 1;
        }

        totalAnimationDurationTime = spinZoomPackage.totalDuration;
        startFadeOutTime =  spinZoomPackage.startFadeInTime;
        startZoomTime = spinZoomPackage.startZoomInAnimation;
        startRotationTime = spinZoomPackage.startRotationInTime;


*/

////   public bool startAnimation = false;
////   public Vector3 targetZoom;
////   public float rotationDegrees;
////   public float rotationTime;
////   public float totalDurationTime;
////   public float fadeoutTime;
////   //public float scaleSpeed;

////   private float zoomPerTick;

////   private float rotationPerTick;
////   private float rotationCounter;
////   private float totalDurationCounter;
////   private float transparentDurationCounter;
////   private float fadeoutCounter;
////// Use this for initialization
////void Start ()
////{
////    gameObject.transform.localScale = Vector3.zero;
////       rotationPerTick = rotationDegrees / rotationTime;
////    rotationCounter = rotationTime;
////    totalDurationCounter = totalDurationTime;
////    zoomPerTick = targetZoom.x/rotationTime;
////    fadeoutCounter = fadeoutTime;


////}

////// Update is called once per frame
////void Update () {

////    if (startAnimation)
////    {
////           totalDurationCounter -= Time.deltaTime;
////           rotationCounter -= Time.deltaTime;

////        if (totalDurationCounter <= 0.0f)
////        {
////            startAnimation = false;
////            GetComponent<SpriteRenderer>().material.color = Color.clear;
////        }
////        if (rotationCounter >= 0.0f)
////        {
////            transform.localScale = Vector3.Lerp(transform.localScale, targetZoom, zoomPerTick * Time.deltaTime);
////            Vector3 rotation = transform.rotation.eulerAngles;
////            rotation.z += rotationPerTick * Time.deltaTime;
////               transform.rotation = Quaternion.Euler(rotation);
////        }
////        if (rotationCounter <= 0.0f)
////        {
////            Color color = GetComponent<SpriteRenderer>().material.color;
////            color.a = color.a - Time.deltaTime/fadeoutTime;
////            GetComponent<SpriteRenderer>().material.color = color;
////        }
////    }

//}




/*  working version before redesign:
    //private float totalAnimationDurationTime;
//private float startFadeOutTime;
//private float startZoomTime;
//private float startRotationTime;

//public Vector3 targetScale;
//public float rotationDegrees;

//private float waitTimeForFadeOut;
//private float fadeOutSteps;

//private float waitTimeForZoom;
//private float zoomSteps;

//private float waitTimeForInRotation;
//private float waitTimeForOutRotation;
//private float rotationInSteps;
//private float rotationOutSteps;

//private float totalAnimationDurationCounter;
//private float startFadeOutCounter;
//private float startZoomCounter;
//private float rotationInCounter;

//public bool startAnimation = false;
//public bool reverseAnimation = false;

//public bool dirty = false;

//void Awake()
//{

//}

//void Start()
//{

//}

//void Update()
//{
//    if (startAnimation)
//    {
//        waitTimeForFadeOut -= Time.deltaTime;
//        waitTimeForInRotation -= Time.deltaTime;
//        waitTimeForZoom -= Time.deltaTime;
//        if (waitTimeForZoom <= 0.0f)
//        {
//            ZoomImage();
//        }
//        if (waitTimeForInRotation <= 0.0f)
//        {
//            RotateImage();
//        }
//        if (waitTimeForFadeOut <= 0.0f)
//        {
//            FadeOutImage();
//        }

//    }
//    if (dirty)
//    {
//        StartAnimation();
//        dirty = false;
//        GetComponent<SpriteRenderer>().material.color = Color.white;
//        transform.localScale = Vector3.zero;
//        transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
//    }

//}

//private void ZoomImage()
//{
//    startZoomCounter -= Time.deltaTime;
//    if (startZoomCounter >= 0.0f)
//    {
//        transform.localScale = Vector3.Lerp(transform.localScale, targetScale, zoomSteps * Time.deltaTime);
//    }

//}

//private void RotateImage()
//{
//    rotationInCounter -= Time.deltaTime;
//    if (rotationInCounter >= 0.0f)
//    {
//        Vector3 rotation = transform.rotation.eulerAngles;
//        if (spinZoomPackage.rotateClockwise)
//        {
//            rotation.z += rotationInSteps*Time.deltaTime;
//        }
//        else
//        {
//            rotation.z -= rotationInSteps * Time.deltaTime;
//        }
//        transform.rotation = Quaternion.Euler(rotation);
//    } else // (rotationInCounter < 0.0f)
//    {
//        // 

//    }
//}

//private void FadeOutImage()
//{
//    startFadeOutCounter -= Time.deltaTime;
//    if (startFadeOutTime >= 0.0f)
//    {
//        Color color = GetComponent<SpriteRenderer>().material.color;
//        color.a -= fadeOutSteps*Time.deltaTime;
//        GetComponent<SpriteRenderer>().material.color = color;
//    }
//}


//public void SetAnimationParameters(float pTotalDuration, float pStartTimeZoom, float pStartTimeRotation, float pStartTimeFadeOut,
//    float pRotationDegrees, float pTargetZoom)
//{
//    totalAnimationDurationTime = pTotalDuration;
//    startFadeOutTime = pStartTimeFadeOut;
//    startZoomTime = pStartTimeZoom;
//    startRotationTime = pStartTimeRotation;
//    rotationDegrees = pRotationDegrees;
//    targetScale = new Vector3(pTargetZoom, pTargetZoom, pTargetZoom);

//}

//public void StartAnimation()
//{
//    startAnimation = true;
//    totalAnimationDurationCounter = spinZoomPackage.totalDuration;

//    startFadeOutCounter = spinZoomPackage.totalDuration - spinZoomPackage.startFadeInTime - spinZoomPackage.fadeOutTime;
//    rotationInCounter = spinZoomPackage.totalDuration - spinZoomPackage.startRotationInTime - spinZoomPackage.rotateOutTime;
//    startZoomCounter = spinZoomPackage.totalDuration - spinZoomPackage.startZoomInAnimation - spinZoomPackage.zoomOutTime;

//    waitTimeForFadeOut = spinZoomPackage.fadeOutTime;
//    waitTimeForInRotation = spinZoomPackage.startRotationInTime;
//    waitTimeForOutRotation = spinZoomPackage.rotateOutTime;
//    waitTimeForZoom = spinZoomPackage.startZoomInAnimation;
//    rotationInSteps = spinZoomPackage.totalDegreesToRotateIn / (spinZoomPackage.totalDuration - spinZoomPackage.startRotationInTime - spinZoomPackage.rotateOutTime);
//    rotationOutSteps = spinZoomPackage.totalDegreesToRotateOut / (spinZoomPackage.rotateOutTime);
//    if (spinZoomPackage.zoomToTarget == 0.0f)
//    {
//        spinZoomPackage.zoomToTarget = 1.0f;
//    }
//    zoomSteps = (spinZoomPackage.totalDuration - spinZoomPackage.startZoomInAnimation) / spinZoomPackage.zoomToTarget;
//    if (spinZoomPackage.fadeOutTime != 0.0f)
//    {
//        fadeOutSteps = 1.0f / (spinZoomPackage.totalDuration - spinZoomPackage.fadeOutTime - spinZoomPackage.startFadeInTime);
//    }
//    else
//    {
//        fadeOutSteps = 1;
//    }

//    totalAnimationDurationTime = spinZoomPackage.totalDuration;
//    startFadeOutTime = spinZoomPackage.startFadeInTime;
//    startZoomTime = spinZoomPackage.startZoomInAnimation;
//    startRotationTime = spinZoomPackage.startRotationInTime;
//}

    */