﻿using UnityEngine;
using System.Collections;
using System.IO;
using Assets.Scripts;

public class ModifyTray : MonoBehaviour {

    private bool selected = false;
    private bool locked = false;

    public GameObject normalTextObject;
    public GameObject cooldownTextObject;
    public GameObject selectionObject;
    private Tray tray = new Tray();
    private Texture2D texture = null;
    private Vector3 offset = Vector3.zero;

    // Use this for initialization
    void Start () {
	    selectionObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	    if (locked && Input.GetKeyDown(KeyCode.Space))
	    {
	        locked = false;
	    }
        if (selected || locked)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition) + offset;
            if (!locked)
            {
                pos.z = gameObject.transform.position.z;
                gameObject.transform.position = pos;
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                locked = true;
            }

            if (Input.GetKey(KeyCode.R))
            {
                transform.localScale += new Vector3(0.5f * Time.deltaTime, 0.5f * Time.deltaTime, 0.5f * Time.deltaTime);
                //normalTextObject.transform.localScale -= new Vector3(0.5f * Time.deltaTime, 0.5f * Time.deltaTime, 0.5f * Time.deltaTime);
                //cooldownTextObject.transform.localScale -= new Vector3(0.5f * Time.deltaTime, 0.5f * Time.deltaTime, 0.5f * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.Q))
            {
                if (transform.localScale.x > 0.1f && transform.localScale.y > 0.1f)
                {
                    transform.localScale -= new Vector3(0.5f * Time.deltaTime, 0.5f * Time.deltaTime, 0.5f * Time.deltaTime);
                }
            }

            if (Input.GetKey(KeyCode.F))
            {
                if (transform.localScale.x + normalTextObject.transform.localScale.x > 0.1f && transform.localScale.y + normalTextObject.transform.localScale.y > 0.1f)
                {
                    normalTextObject.transform.localScale -= new Vector3(0.5f * Time.deltaTime, 0.5f * Time.deltaTime, 0.5f * Time.deltaTime);
                    cooldownTextObject.transform.localScale -= new Vector3(0.5f * Time.deltaTime, 0.5f * Time.deltaTime, 0.5f * Time.deltaTime);
                } 
            } else if (Input.GetKey(KeyCode.G))
            {
                normalTextObject.transform.localScale += new Vector3(0.5f * Time.deltaTime, 0.5f * Time.deltaTime, 0.5f * Time.deltaTime);
                cooldownTextObject.transform.localScale += new Vector3(0.5f * Time.deltaTime, 0.5f * Time.deltaTime, 0.5f * Time.deltaTime);
            }

            if (Input.GetKeyDown(KeyCode.Delete))
            {
                GameObject.Destroy(gameObject);
            }

            if (Input.GetKey(KeyCode.C))
            {
                Vector3 rotate = transform.rotation.eulerAngles;
                rotate.z += 45.0f * Time.deltaTime;
                transform.rotation = Quaternion.Euler(rotate);
                Vector3 rotateText = normalTextObject.transform.rotation.eulerAngles;
                rotateText.z -= 45.0f*Time.deltaTime;
                normalTextObject.transform.rotation = Quaternion.Euler(rotateText);
                cooldownTextObject.transform.rotation = Quaternion.Euler(rotateText);
            }
            else if (Input.GetKey(KeyCode.Z))
            {
                Vector3 rotate = transform.rotation.eulerAngles;
                rotate.z -= 45.0f * Time.deltaTime;
                transform.rotation = Quaternion.Euler(rotate);
                Vector3 rotateText = normalTextObject.transform.rotation.eulerAngles;
                rotateText.z += 45.0f * Time.deltaTime;
                normalTextObject.transform.rotation = Quaternion.Euler(rotateText);
                cooldownTextObject.transform.rotation = Quaternion.Euler(rotateText);
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                Vector3 rotate = transform.rotation.eulerAngles;
                rotate.z = (int)rotate.z;
                int clamper = (int)rotate.z / 15;
                rotate.z = clamper * 15;
                Vector3 normalTextRotate = normalTextObject.transform.rotation.eulerAngles;
                normalTextRotate.z = 0.0f;
                transform.rotation = Quaternion.Euler(rotate);
                normalTextObject.transform.rotation = Quaternion.Euler(normalTextRotate);
                cooldownTextObject.transform.rotation = Quaternion.Euler(normalTextRotate);
            }
            if (Input.GetKeyDown(KeyCode.Minus) || Input.GetKeyDown(KeyCode.KeypadMinus))
            {
                GetComponent<SpriteRenderer>().sortingOrder -= 1;
                normalTextObject.GetComponent<TextureMeshDrawPriority>().orderInLayer = GetComponent<SpriteRenderer>().sortingOrder + 2;
                if (cooldownTextObject.activeSelf)
                {
                    cooldownTextObject.GetComponent<TextureMeshDrawPriority>().orderInLayer = normalTextObject.GetComponent<TextureMeshDrawPriority>().orderInLayer + 1;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Plus) || Input.GetKeyDown(KeyCode.KeypadPlus))
            {
                GetComponent<SpriteRenderer>().sortingOrder += 1;
                normalTextObject.GetComponent<TextureMeshDrawPriority>().orderInLayer = GetComponent<SpriteRenderer>().sortingOrder + 2;
                if (cooldownTextObject.activeSelf)
                {
                    cooldownTextObject.GetComponent<TextureMeshDrawPriority>().orderInLayer = normalTextObject.GetComponent<TextureMeshDrawPriority>().orderInLayer + 1;
                }

            }

            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.W))
            {
                GetComponent<TrayAnimateOn>().SetAnimationDirection(TrayDirection.Up);
                GetComponent<TrayAnimateOff>().SetAnimationDirection(TrayDirection.Up);
            } else if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.A))
            {
                GetComponent<TrayAnimateOn>().SetAnimationDirection(TrayDirection.Left);
                GetComponent<TrayAnimateOff>().SetAnimationDirection(TrayDirection.Left);
            }
            else if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.S))
            {
                GetComponent<TrayAnimateOn>().SetAnimationDirection(TrayDirection.Down);
                GetComponent<TrayAnimateOff>().SetAnimationDirection(TrayDirection.Down);
            }
            else if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.D))
            {
                GetComponent<TrayAnimateOn>().SetAnimationDirection(TrayDirection.Right);
                GetComponent<TrayAnimateOff>().SetAnimationDirection(TrayDirection.Right);
            }
            // TextMove
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.W))
            {
                //if (normalTextObject != null)
                //{
                    Vector3 textObjPos = normalTextObject.transform.position;
                    textObjPos.y += 1.0f * Time.deltaTime;
                    normalTextObject.transform.position = textObjPos;
                //}
            }
            else if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.A))
            {
                //if (normalTextObject != null)
                //{
                    Vector3 textObjPos = normalTextObject.transform.position;
                    textObjPos.x -= 1.0f * Time.deltaTime;
                    normalTextObject.transform.position = textObjPos;
                //}
            }
            else if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.S))
            {
                //if (normalTextObject != null)
                //{
                    Vector3 textObjPos = normalTextObject.transform.position;
                    textObjPos.y -= 1.0f * Time.deltaTime;
                    normalTextObject.transform.position = textObjPos;
                //}
            }
            else if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.D))
            {
                //if (normalTextObject != null)
                //{
                    Vector3 textObjPos = normalTextObject.transform.position;
                    textObjPos.x += 1.0f * Time.deltaTime;
                    normalTextObject.transform.position = textObjPos;
                //}
            }
            // cooldown move
            if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKey(KeyCode.W))
            {
                Vector3 cooldownPos = cooldownTextObject.transform.position;
                cooldownPos.y += 1.0f*Time.deltaTime;
                cooldownTextObject.transform.position = cooldownPos;
            }
            else if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKey(KeyCode.A))
            {
                Vector3 cooldownPos = cooldownTextObject.transform.position;
                cooldownPos.x -= 1.0f * Time.deltaTime;
                cooldownTextObject.transform.position = cooldownPos;
            }
            else if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKey(KeyCode.S))
            {
                Vector3 cooldownPos = cooldownTextObject.transform.position;
                cooldownPos.y -= 1.0f * Time.deltaTime;
                cooldownTextObject.transform.position = cooldownPos;
            }
            else if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKey(KeyCode.D))
            {
                Vector3 cooldownPos = cooldownTextObject.transform.position;
                cooldownPos.x += 1.0f * Time.deltaTime;
                cooldownTextObject.transform.position = cooldownPos;
            }


        }
    }

    void OnMouseUp()
    {
        selectionObject.SetActive(false);
        selected = false;
        GetComponent<TrayAnimateOff>().SetStartPosition(transform.position);
        GetComponent<TrayAnimateOn>().SetEndPosition(transform.position);
        tray.trayTransform.position = new MyVector3(transform.position);
        tray.trayTransform.rotation = new MyQuaternion(transform.rotation);
        tray.trayTransform.scale = new MyVector3(transform.localScale);
        tray.trayTransform.sortingOrder = GetComponent<SpriteRenderer>().sortingOrder;
        bool found = false;
        for (int i = 0; i < SaveObject.Instance.currentPreset.listOfTrays.Count; i++)
        {
            if (tray.trayID == SaveObject.Instance.currentPreset.listOfTrays[i].trayID)
            {
                SaveObject.Instance.currentPreset.listOfTrays[i] = tray;
                found = true;
            }
        }
        if (!found)
        {
            for (int i = 0; i < SaveObject.Instance.currentPreset.listOfTrays.Count; i++)
            {
                if (SaveObject.Instance.currentPreset.listOfTrays[i].trayID == tray.trayID)
                {
                    SaveObject.Instance.currentPreset.listOfTrays[i] = tray;
                    found = true;
                }
            }
        }
        if (!found)
        {
            SaveObject.Instance.currentPreset.listOfTrays.Add(tray);
        }
        
    }

    private void OnMouseDown()
    {
        selected = true;
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z));
        selectionObject.SetActive(true);
        Debug.Log("OnMouseDown");

        if (texture == null)
        {
            Texture2D imageTexture = GetComponent<SpriteRenderer>().sprite.texture;
            texture = new Texture2D(imageTexture.width, imageTexture.height);
            Color[] colors = new Color[texture.width * texture.height];
            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = Color.green;
                colors[i].a = 0.25f;
            }
            texture.SetPixels(colors);
            texture.Apply();
            Sprite sprite = Sprite.Create(texture, GetComponent<SpriteRenderer>().sprite.textureRect, new Vector2(0.5f, 0.5f));
            sprite.name = "Tray" + tray.trayID + Path.GetFileNameWithoutExtension(tray.trayFilename);
            selectionObject.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1002;
            selectionObject.GetComponent<SpriteRenderer>().sprite = sprite;
            selectionObject.transform.position = transform.position;
            selectionObject.transform.rotation = transform.rotation;
        }



    }

    public GameObject GetTextGameObject()
    {
        return normalTextObject;
    }

    public GameObject GetCooldownGameObject()
    {
        return cooldownTextObject;
    }

    public void SetTray(Tray pTray)
    {
        tray = pTray;
        normalTextObject.GetComponent<TextMesh>().text = tray.trayText;
        normalTextObject.GetComponent<TextMesh>().color = new Color(tray.trayTextColor.r, tray.trayTextColor.g, tray.trayTextColor.b);
        normalTextObject.GetComponent<TextureMeshDrawPriority>().orderInLayer = tray.trayTransform.sortingOrder + 1;

        cooldownTextObject.SetActive(tray.trayUseCountdown);
        if (tray.trayUseCountdown)
        {
            cooldownTextObject.GetComponent<CooldownCounter>().SetCooldown(tray.trayCountdownMinutes);
            cooldownTextObject.GetComponent<TextMesh>().color = new Color(tray.trayTextColor.r, tray.trayTextColor.g, tray.trayTextColor.b);
            cooldownTextObject.GetComponent<TextureMeshDrawPriority>().orderInLayer = tray.trayTransform.sortingOrder + 2;
        }
        GetComponent<TrayAnimateOn>().SetAnimationDirection(tray.trayDirection);
        GetComponent<TrayAnimateOff>().SetAnimationDirection(tray.trayDirection);
        GetComponent<TrayAnimateOn>().SetAnimationSpeed(tray.trayFadeInSpeed);
        GetComponent<TrayAnimateOff>().SetAnimationSpeed(tray.trayFadeOutSpeed);
        GetComponent<TrayAnimateOn>().StartAnimation();
        GetComponent<TrayAnimateOff>().StopAnimation();
        GetComponent<TrayAnimateOn>().SetEndPosition(tray.trayTransform.GetPositionAsVector3());
        transform.position = tray.trayTransform.GetPositionAsVector3();
        transform.localScale = tray.trayTransform.GetScaleAsVector3();
        transform.rotation = tray.trayTransform.GetRotationAsQuaternion();
        GetComponent<SpriteRenderer>().sortingOrder = tray.trayTransform.sortingOrder;

    }

    public Tray GetTray()
    {
        return tray;
    }

}
