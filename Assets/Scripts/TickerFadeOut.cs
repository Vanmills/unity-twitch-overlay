﻿using UnityEngine;
using System.Collections;
using TwitchOverlay;

#pragma warning disable 0108

public class TickerFadeOut : MonoBehaviour
{
    private float fadeoutSpeed = 0.8f;
    private bool startTransition = false;

    //public MeshRenderer textRenderer;
    private SpriteRenderer renderer;
    //private Material material;
    private Color originalColor;
    private bool dirty = true;

    void Awake()
    {
        TwitchOverlay.EventHandler.OnFadeOutInterface += new EventHandler.NoArgDelegate(ChangeFading);
        renderer = gameObject.GetComponent<SpriteRenderer>();
        originalColor = new Color(renderer.color.r, renderer.color.g, renderer.color.b, renderer.color.a);
    }


	// Use this for initialization
	void Start ()
	{
        
    }
	
	// Update is called once per frame
	void Update ()
	{

           
	    if (startTransition)
	    {
            if (dirty)
            {
                dirty = false;
                SaveObject.Instance.currentPreset.useTickerBackground = !startTransition;
            }
            renderer.color = Color.Lerp(renderer.color, Color.clear, fadeoutSpeed * Time.deltaTime);
        }
        else
        {
            if (dirty)
            {
                dirty = false;
                SaveObject.Instance.currentPreset.useTickerBackground = !startTransition;
            }
            renderer.color = Color.Lerp(renderer.color, originalColor, fadeoutSpeed * Time.deltaTime);
        }
    }


    public void ChangeFading()
    {
        dirty = true;
        startTransition = !startTransition;
    }
}
