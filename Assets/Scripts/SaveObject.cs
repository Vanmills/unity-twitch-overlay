﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class SaveObject
{
    [NonSerialized]
    private static volatile SaveObject instance;
    [NonSerialized]
    private static object instanceLock = new object();

    private SaveObject() { }
    public static SaveObject Instance
    {
        get
        {
            if (instance == null)
            {
                lock (instanceLock)
                {
                    instance = new SaveObject();
                }
            }
            return instance;
        }
    }
    // this is used to save data on the current preset
    // listOfPresets[0] is the default preset
    // rest are different. 
    public Preset currentPreset = new Preset();
    // We only save this
    public List<Preset> listOfPresets = new List<Preset>();

}


//private Preset currentSetup = new Preset();

//public UserColor WindowBGColor = new UserColor();
//public UserColor TickerTextColor = new UserColor();
//public UserColor TickerBackgroundColor = new UserColor();

//public string TickerMessage { get; set; }
//public string GameImage = "";
//public bool TickerOn;
//public bool TickerBarOn;
//public float tickerSpeed;
//public float tickerRepeatTime;
//public List<MyGameObject> AddedImages = new List<MyGameObject>();

//public void LoadSaveData(SaveObject obj)
//{
//    WindowBGColor = obj.WindowBGColor;
//    TickerTextColor = obj.TickerTextColor;
//    TickerBackgroundColor = obj.TickerBackgroundColor;
//    TickerMessage = obj.TickerMessage;
//    TickerBarOn = obj.TickerBarOn;
//    tickerSpeed = obj.tickerSpeed;
//    tickerRepeatTime = obj.tickerRepeatTime;
//    AddedImages = obj.AddedImages;
//}

//public override string ToString()
//{
//    string buildString = "Images count: " + AddedImages.Count + " ";
//    for (int i = 0; i < AddedImages.Count; i++)
//    {
//        buildString += AddedImages[i] + " ";
//    }
//    return buildString;

//}