﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class SmallTickerScroll : MonoBehaviour {

	// Use this for initialization
    public float speed = 0.5f;

    public string smallTickerText = "Hello World";
    public TextMesh textMesh;
    public bool testTicker = false;
    public GameObject ticker1;

	void Start ()
	{
        //textMesh = GetComponent<TextMesh>();
        if (textMesh != null)
        {

            Debug.Log("Textmesh bounds: " + gameObject.GetComponent<MeshRenderer>().bounds);
            Debug.Log("Textmesh calculation: " + textMesh.fontSize * 0.1f * 0.5f * textMesh.text.Length);
            Bounds bounds = textMesh.GetComponent<MeshRenderer>().bounds;
            Debug.Log("Bounds: " + bounds);


            float offsetX = bounds.max.x;
            transform.position = new Vector3(offsetX + 3.0f, transform.position.y, transform.position.z);
        }
    }

    // Update is called once per frame
    void Update ()
	{
        if (transform.position.x <= /*-40.0f */-Mathf.Abs(textMesh.GetComponent<MeshRenderer>().bounds.max.x) )
        {
            GetComponent<TextMesh>().text = smallTickerText;
            transform.position = new Vector3(textMesh.GetComponent<MeshRenderer>().bounds.max.x + 3.0f, transform.position.y, transform.position.z);

        }

        if (textMesh != null)
        {
	        if (testTicker)
	        {
	        Bounds bounds = textMesh.GetComponent<MeshRenderer>().bounds;
	        Vector3 bottomLeftPoint = new Vector3(bounds.min.x, bounds.min.y, bounds.min.z);
            Vector3 Topright = new Vector3(bounds.max.x, bounds.max.y, bounds.max.z);
            Vector3 BottomRight = new Vector3(bounds.max.x, bounds.min.y, bounds.max.z);
            Vector3 TopLeft = new Vector3(bounds.min.x, bounds.max.y, bounds.min.z);
            Debug.DrawLine(bottomLeftPoint, BottomRight, Color.green);
            Debug.DrawLine(bottomLeftPoint, TopLeft, Color.red);
            Debug.DrawLine(TopLeft, Topright, Color.magenta);
            Debug.DrawLine(Topright, BottomRight, Color.black);


	            
	            //testTicker = false;
            }
        }
	    transform.position = new Vector3(transform.position.x - speed * Time.deltaTime, transform.position.y, transform.position.z);


	}
}
