﻿using UnityEngine;
using System.Collections;

public class YoutubeVideoTest : MonoBehaviour
{
    public string youtubeVideoURL;
    public bool dirty = false;

    private bool startVideo = false;
    private WWW webAddress;
    private MovieTexture movieTexture;


	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {

	    if (dirty)
	    {
	        dirty = false;
            webAddress = new WWW(youtubeVideoURL);
            
	        movieTexture = webAddress.movie;
	        int i = 0;
            while (!movieTexture.isReadyToPlay)
            {
                i++;
                if (i == 10000)
                {
                    break;
                }
            }
	        GetComponent<MeshRenderer>().material.mainTexture = movieTexture;
	        startVideo = true;

	    }
	    if (startVideo)
	    {
	        movieTexture.Play();
	        startVideo = false;
	    }
	}

    //IEnumerable LoadWaiting()
    //{
    //    yield return new WaitForSeconds(0.1f);
    //}

    void OnMouseDown()
    {
        
    }

    

}
